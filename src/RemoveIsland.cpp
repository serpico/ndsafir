// -*- mode:c++ -*-

#include "RemoveIsland.h"
#include "CImg.h"
using namespace cimg_library;

namespace ndsafir {

  RemoveIsland::RemoveIsland(Data * data, 
			     Log* logger,
			     ProgressBar * progress_bar):
    Element(data,logger,progress_bar)
  {
    m_threshold=6;
  }


  const char * RemoveIsland::get_name()const {
    return "RemoveIsland";
  }

  int RemoveIsland::get_number_of_steps()const{
    if (m_data)
      return m_data->length();
    else
      return 0;
  }

  void RemoveIsland::set_threshold(const float threshold){
    if (threshold>0){
      m_threshold=threshold;
    }else{
      m_logger->warn("RemoveIsland::Negative threshold. Set to default value.\n");
      m_threshold=6;
    }
  }

  void RemoveIsland::set_to_last(){
    m_iteration=m_data->number_of_iterations()-1;
  }

  void RemoveIsland::set_to_first(){
    m_iteration=0;;
  }

  void RemoveIsland::apply_to_iteration(const int iteration){
    m_progress_bar->set_label("Hot pixel filtering");
    CImg_3x3(I,float);
    for (int ti=0;ti<m_data->length() && !m_progress_bar->is_canceled();++ti){
      CImg<> u(m_data->get_shared_estimate(ti,iteration),true);
      CImg<> v(m_data->get_shared_variance(ti,iteration),true);
      unsigned long num_pts_per_frame=0;
#ifdef cimg_use_openmp
#pragma omp parallel for
#endif
      cimg_forZC(u,z,c)
	cimg_for3x3(u,x,y,z,c,I,float){
	// Compute the diff to the mean of the 3x3 neighborhood (without the central pixel)
	const float mean=(Ipp+Ipc+Ipn+Icp+Icn+Inp+Inc+Inn)/8.0f,delta=Icc-mean;
	// Compute the variance of the 3x3 neighborhood (without the central pixel)
	const float variance=(Ipp*Ipp+Ipc*Ipc+Ipn*Ipn+Icp*Icp+Icn*Icn+Inp*Inp+Inc*Inc+Inn*Inn)/8.0f-(mean*mean);
	if (delta*delta>m_threshold*variance){
	  u(x,y,z,c)=mean;
	  v(x,y,z,c)*=2;
	  num_pts_per_frame++;
	}
      }
      if (num_pts_per_frame>0){
	m_logger->infoline("ndsafir: frame %d : fixing %d pixel ",ti,num_pts_per_frame);
      }
      m_progress_bar->increase();
    }    
    m_logger->infoline("\n");
  }


  void RemoveIsland::apply(){
    apply_to_iteration(m_iteration);
  }

}
