// -*- mode:c++ -*-
/**
   \file Wiener.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/
#include "Wiener.h"
#include "CImg.h"
using namespace cimg_library;

namespace ndsafir {

  Wiener::Wiener(){}

  Wiener::Wiener(Data * data, 
		 Log * logger,
		 ProgressBar * progress_bar):
    Element(data,logger,progress_bar)
  {    
  }

  const char * Wiener::get_name()const{
    return "Wiener";
  }

  int Wiener::get_number_of_steps()const{
    return 0;
  }

  void Wiener::apply() {
    m_progress_bar->set_label("applying wiener filtering");
    const int last=m_data->number_of_iterations()-1;
    for (int t=0;t<m_data->length() && !m_progress_bar->is_canceled();t++){
      CImg<> u1=m_data->get_shared_estimate(t,0);
      CImg<> v1=m_data->get_shared_variance(t,0);
      CImg<> u2=m_data->get_shared_estimate(t,last);
      CImg<> v2=m_data->get_shared_variance(t,last);
#if 0
      const float * ptr_u1=u1.data(),* ptr_v1=v1.data(); 
      float * ptr_u2=u2.data(),* ptr_v2=v2.data();
#ifdef cimg_use_openmp
#pragma omp parallel for
#endif
      for (unsigned int i=0;i<u1.size();++i){
	const float x1=1.f/(*ptr_v1),x2=1.f/(*ptr_v2);
	*ptr_u2=(x1*(*ptr_u1)+x2*(*ptr_u2))/(x1+x2);
	*ptr_v2=(x1+x2)/(x1*x1*+x2*x2);
	ptr_u1++;ptr_v1++;ptr_u2++;ptr_v2++;
      }
#else
      cimg_foroff(u2,i) {
	const float x1=1.f/cimg::max(.5f*v1(i),1.0f),x2=1.f/cimg::max(v2(i),1.0f);
	u2(i)=(x1*u1(i)+x2*u2(i))/(x1+x2);
	v2(i)=(x1+x2)/(x1*x1+x2*x2);
      }
#endif
      m_progress_bar->increase();
    }
  }

}

#ifdef TEST_CSAFIRWIENER
#include "RAMMemoryManager.h"
#include "Log.h"
#include "ProgressBar.h"
#include "Data.h"
int main(void){
  RAMMemoryManager * data = new RAMMemoryManager;
  data->assign(10,10,10,3,10,2);
  for (int i=0;i<data->number_of_iterations();i++)
    for (int t=0;t<data->length();t++){
      data->get_shared_estimate(t,i)=(float)(100*i);
      data->get_shared_variance(t,i)=1.f;
    }
  Log * logger = new Log;
  ProgressBar * progress_bar = new ProgressBar;
  Wiener * wiener = new Wiener(data,logger,progress_bar);
  wiener->apply();
  if (data->get_shared_estimate(0,1)(5,5,5)==50) printf("Wiener::OK\n");
  else {    
    printf("Wiener::FAILED\n");
    printf("value is :%f\n",data->get_shared_estimate(0,1)(5,5,5));
  }
  return 0;
}
#endif
