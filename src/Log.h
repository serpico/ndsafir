// -*- mode:c++ -*-
/**
   \file Log.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef LOG_H
#define LOG_H
#include <cstdio>

namespace ndsafir {
  //! A logging system
  class Log{
  public:
    Log();
    ~Log();
    virtual void set_verbosity(int level);

    virtual int get_verbosity()const;

    virtual void set_log_file(std::FILE * log_file);

    virtual void error(const char *format,...);
  
    virtual void warn(const char *format,...);

    virtual void info(const char *format,...);

    virtual void infoline(const char *format,...);
  
  private:
    //! Output stream for warning and comments
    /*!
      Can be useful to redirect information to a file instead of the standard output (stdout)
    */
    std::FILE * m_log_file;
    //! indicate the verbose mode
    int m_verbosity;
  };
}
#endif
