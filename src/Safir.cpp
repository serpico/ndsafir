// -*- mode:c++ -*-
/**
   \file .h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
   copyright(c) 2003-2006 by INRA-MIA Jouy-en-Josas and INRIA Rennes. All Rights Reserved. 
**/

#include "Safir.h"
#include "CImg.h" 
using namespace cimg_library;
using namespace cimg;

#include "loops_def.h"

namespace ndsafir {

  Safir::Safir(Data * data, 
	       Log * logger,
	       ProgressBar * progress_bar):
    Element(data,logger,progress_bar)
  { 
    m_lepski_threshold=4.f;
    m_patch_width=4;    m_patch_height=4;    m_patch_depth=0;
    m_sampling_width=5; m_sampling_height=5; m_sampling_depth=1;
    m_kernel_pvalue=1e-2; m_use_lookup=true;   
  }

  const char * Safir::get_name()const{
    return "Safir";
  }

  int Safir::get_number_of_steps()const{
    if (m_data)
      return m_data->length()*(m_data->number_of_iterations()-1);
    else
      return 0;
  }

  void Safir::set_patch_size(const int rx, const int ry, const int rz){  
    m_patch_width=rx;      m_patch_height=ry;      m_patch_depth=rz;
    m_sampling_width=rx+1; m_sampling_height=ry+1; m_sampling_depth=rz+1;
    m_logger->info("ndsafir: set patch size to %dx%dx%d\n",2*rx+1,2*ry+1,2*rz+1);
  }

  void Safir::set_kernel_pvalue(const float val){
    m_kernel_pvalue = max(min((double)val, 1.0), 0.0);
    const double x = 2.0*(1.0-m_kernel_pvalue)-1.0,
      a = 8.0*(cimg::PI-3.0) / (3.0 * cimg::PI*(4.0 - cimg::PI)),
      b = std::log(1.0 - x * x),
      c = 2.0 / (cimg::PI * a) + b /2.0,
      signx = (x > 0.0 ? 1.0 : -1.0),
      erfinv = signx*sqrt(sqrt( c * c - b / a) - c);
    m_kernel_quantile = std::sqrt(2.0) * erfinv;
    m_logger->info("ndsafir: set kernel pvalue to %g\n", m_kernel_pvalue);
  }

  void Safir::set_use_lookup(const bool use_lookup){
    m_use_lookup=use_lookup;
    m_logger->info("ndsafir: %s look up table\n",m_use_lookup?"using":"not using");
  }

  inline float Safir::get_kernel_bandwidth(int n)const{    
    return (float)(0.5 * std::pow((double)m_kernel_quantile + std::sqrt(2.0 * ((double)n - 1.0) - 1.0), 2.0));
  }
 
  void Safir::set_lepski_threshold(float val){
    m_lepski_threshold = max(0.f,val);
    m_logger->info("ndsafir: set lepskii threshold to %f\n", m_lepski_threshold);
  }

  int Safir::get_search_zone_width(const int k)const {
    if (m_data->length() != 1)
      return pow(2.,ceil((k-2.)/2.));
    else
      return pow(2.,k-1.);
  }

  int Safir::get_search_zone_height(const int k)const {
    if (m_data->length() != 1)
      return pow(2.,ceil((k-2.0)/2.0));
    else
      return pow(2.,k-1.);
  }

  int Safir::get_search_zone_depth(const int k)const {
    if (m_data->depth() != 1){
      if (m_data->length()!=1) { 
	return pow(2., ceil((k-2.0)/2.0)); 
      } else { 
	return pow(2., k-1.); 
      }
    } else{ 
      return 0; 
    }
  }

  int Safir::get_search_zone_length(const int k)const{
    if (m_data->length()!=1)
      return floor((float)k/2.);
    else
      return 0;
  }

  int Safir::get_max_iter()const{
    return m_data->number_of_iterations();
  }

  void Safir::compute_mean_variance(const CImg<> &img, CImg<> & mean, CImg<> & var, int k)const{
    if (m_use_lookup && k>2){
      const int n=(2*m_patch_width+1)*(2*m_patch_height+1)*(2*m_patch_depth+1);
      CImg<> 
	mx(2*m_patch_width+1,1,1,1,1),
	my(1,2*m_patch_height+1,1,1,1),
	mz(1,1,2*m_patch_depth+1,1,1);
      mean.assign(img,false).convolve(mx).convolve(my).convolve(mz);
      var.assign(img.get_pow(2)).convolve(mx).convolve(my).convolve(mz);
      mean/=n;   var/=n;   var-=mean.get_pow(2);
    }
  }

  void Safir::init_lists(CImgList<> &u0,CImgList<> &v0,                   
			 CImgList<> &u1,CImgList<> &v1, 
			 CImgList<> &ub,CImgList<> &vb,
			 const int k)const{
    const int wt = get_search_zone_length(k), dti = 2 * wt +  1;
    u0.assign(dti,m_data->width(),m_data->height(),m_data->depth(),m_data->spectrum(),0);    
    v0.assign(dti,m_data->width(),m_data->height(),m_data->depth(),m_data->spectrum(),0);
    u1.assign(dti,m_data->width(),m_data->height(),m_data->depth(),m_data->spectrum(),0);    
    v1.assign(dti,m_data->width(),m_data->height(),m_data->depth(),m_data->spectrum(),0);
    if (m_use_lookup && k>2){
      ub.assign(dti,m_data->width(),m_data->height(),m_data->depth(),m_data->spectrum(),0);  
      vb.assign(dti,m_data->width(),m_data->height(),m_data->depth(),m_data->spectrum(),0);
    }
    for (int tj = 0; tj < wt; tj++) {
      const int xtj = tj + wt; //indexes of the frames j      
      u0[xtj].assign(m_data->get_shared_estimate(tj, 0), true);
      v0[xtj].assign(m_data->get_shared_variance(tj, 0), true);
      u1[xtj].assign(m_data->get_shared_estimate(tj, k-1), true);
      v1[xtj].assign(m_data->get_shared_variance(tj, k-1), true);
      compute_mean_variance(u1[xtj], ub[xtj], vb[xtj], k);
    }    
  }

  void Safir::load_front_frame(CImgList<> &u0, CImgList<> &v0,                   
			       CImgList<> &u1, CImgList<> &v1, 
			       CImgList<> &ub, CImgList<> &vb,
			       const int k, const int ti)const{    
    if (false){
      const int 
	wt =  get_search_zone_length(k),
	xtj = 2 * wt + 1,// index of the front frame in the array (2wt+1) -1	
	tjmax = u0.size() - 1; // frame number of the front frame
      if(tjmax < m_data->length()) { // do not load the front frame if we reached the end
	u0[xtj].assign(m_data->get_shared_estimate(tjmax, 0), true);
	v0[xtj].assign(m_data->get_shared_variance(tjmax, 0), true);
	u1[xtj].assign(m_data->get_shared_estimate(tjmax, k - 1), true);
	v1[xtj].assign(m_data->get_shared_variance(tjmax, k - 1), true);
	compute_mean_variance(u1[xtj], ub[xtj], vb[xtj], k);
      }
    }else{ // if u0 u1 etc are shared we need to reload them instead of roll
      const int wt =  get_search_zone_length(k);
      const int tj0 = max(0, ti - wt), tj1 = min(m_data->length() - 1, ti + wt);
      for (int tj = tj0; tj <= tj1; tj++){ // for each point in the seach zone
	const int xtj = tj - ti + wt; //indexes of the frames j
	u0[xtj].assign(m_data->get_shared_estimate(tj, 0), true);
	v0[xtj].assign(m_data->get_shared_variance(tj, 0), true);
	u1[xtj].assign(m_data->get_shared_estimate(tj, k - 1), true);
	v1[xtj].assign(m_data->get_shared_variance(tj, k - 1), true);
	compute_mean_variance(u1[xtj], ub[xtj], vb[xtj], k);
      }
    }   
  }

  void Safir::roll_lists(CImgList<> &u0, CImgList<> &v0,
			 CImgList<> &u1, CImgList<> &v1,
			 CImgList<> &ub, CImgList<> &vb,
			 const int k)const{
    const int wt = get_search_zone_length(k), dti = 2 * wt + 1;    
    for (int i = 0; i < dti - 1; ++i){
      if (false){ // we don't need to roll these ones if they are shared
	cimg::swap(u0[i], u0[i+1]);
	cimg::swap(v0[i], v0[i+1]);
	cimg::swap(u1[i], u1[i+1]);
	cimg::swap(v1[i], v1[i+1]);
      }
      if (m_use_lookup && k > 2){
	cimg::swap(ub[i], ub[i+1]);
	cimg::swap(vb[i], vb[i+1]);
      }
    }
  }

   float Safir::distance_patch(const CImgList<float> &u,const CImgList<float>&v,
				     const CImgList<float> &ub,const CImgList<float>&vb,
				     const int xi,const int yi,const int zi,const int ti,
				     const int xj,const int yj,const int zj,const int tj, 
				     const int k)const{

    const int 
      wt = get_search_zone_length(k), // radius in time of the neighborhood
      xti = wt,                      // index of the frame ti
      xtj = tj - ti + wt;           //index of the frame tj

    if (m_use_lookup && !ub.is_empty()){ // block related to the patch prefiltering (lookup)
      float dm=0; // test on the mean of the patches
      cimg_forC(u[xti],c){ // for each channel
	const float val=ub[xti](xi,yi,zi,c) - ub[xtj](xj,yj,zj,c); // difference of mean
	dm += val * val / vb[xti](xi,yi,zi,c); // normalized by the variance
      }      
      if (dm > .01*(2*m_patch_width+1)*(2*m_patch_height+1)*(2*m_patch_depth+1)) {
	return 1e6f; // if too big returns a large value to get a 0 weight
      }
      bool dv = false; // test on the variance of the patches
      cimg_forC(u[xti],c){ // for each channel
	const float tvi = vb[xti](xi,yi,zi,c), tvj = vb[xtj](xj,yj,zj,c);
	dv=(dv || max(tvi,tvj) / min(tvi,tvj)<8.f); // fisher test
      }
      if (!dv) {return 1e6f;} // if one failed returns a large value
    }

    float d = 0;  // distance
    float n = 0;  // number of points in the patch
    cimg_forC(u[xti], c){ // for each channel
      cimg_forXYZ_patch(u[xti],xi,yi,zi,xj,yj,zj,xi_,yi_,zi_,xj_,yj_,zj_,
			m_patch_width,m_patch_height,m_patch_depth){ // for i,j in the patch
	const float 
	  val = u[xti](xi_,yi_,zi_,c) - u[xtj](xj_,yj_,zj_,c),
	  vi = v[xti](xi_,yi_,zi_,c), vj = v[xtj](xj_,yj_,zj_,c);
	d += val * val * (vi+vj) / (vi*vj); // compute the normalized distance
	n++;                      // increase the number of points in the patch
      }
    }
    return (.5f * d) / get_kernel_bandwidth(n);// 0.5 comes from the variance computation
  }

  void Safir::accumulate_patch(CImg<float> & u1, CImg<float> & v1, CImg<float> & sw, 
			       const CImgList<float> & u0, const CImgList<float> & v0, 
			       const int xi,const int yi,const int zi,const int ti,
			       const int xj,const int yj,const int zj,const int tj,
			       const float weight, const int k)const{
    if (weight>0.0f){ // if the weight is not 0

      const int 
	wt = get_search_zone_length(k), // radius in time of the neighborhood
	xtj = tj - ti + wt;           //index of the frame tj

      cimg_forXYZ_patch(u1,xi,yi,zi,xj,yj,zj,xi_,yi_,zi_,xj_,yj_,zj_, // for i,j in the patch
			m_patch_width,m_patch_height,m_patch_depth){
	cimg_forC(u1, c){                                                // for each channel
	  u1(xi_,yi_,zi_,c) += weight * u0[xtj](xj_, yj_, zj_, c);         // accumulate the estimate
	  v1(xi_,yi_,zi_,c) += 2.f * weight * weight * v0[xtj](xj_, yj_, zj_, c); // and the variance
	}
	sw(xi_,yi_,zi_) += weight; // sums up the weights for later normalization
      }

    }
  }

  void Safir::normalize_step(CImg<> &u2, CImg<> &v2, CImg<> &sw, CImg<imap_pixel_t>&imap)const{  
    cimg_forXYZC(u2, x, y, z, c){ // for each pixel      
      if (imap(x,y,z) == 0 && sw(x,y,z) > 0){ // if active and weights are not 0
	const float a = 1.f/sw(x,y,z);
	u2(x, y, z, c) *= a;   // u2/=sw
	v2(x, y, z, c) *= a*a; // v2/=sw*sw
      }
    }
  }

  void Safir::control_step(CImg<> &u2, CImg<> &v2, CImg<imap_pixel_t> & imap,
			   CImgList<> &u1, CImgList<> &v1, const int t, const int k)const{
    const int wt = get_search_zone_length(k);
    cimg_foroff(u2,i) {
      float x1 = 1.f / max(v1[wt](i), 1e-12f), x2 = 1.f / max(v2(i), 1e-12f);
      u2(i) = (x1 * u1[wt](i) + x2*u2(i)) / (x1+x2);
      v2(i) = (x1+x2) / (x1*x1+x2*x2);
    }
    if (k>1){
      for (int kp = 0; kp < k - 1; kp++){
	const CImg<> 
	  up(m_data->get_shared_estimate(t, kp),true),
	  vp(m_data->get_shared_variance(t, kp),true);
	cimg_forXYZ(imap,x,y,z){
	  if (imap(x,y,z)==0){
	    float s=0;
	    cimg_forC(u2,c){
	      const float tmp = up(x,y,z,c) - u2(x,y,z,c);
	      s += (tmp*tmp)/vp(x,y,z,c);
	    }
	    if(s > m_lepski_threshold * m_lepski_threshold){
	      imap(x,y,z) = k;
	      cimg_forC(u2,c){ 
		u2(x,y,z,c) = u1[wt](x,y,z,c); 
		v2(x,y,z,c) = v1[wt](x,y,z,c); 
	      }
	    }
	  }else{
	    cimg_forC(u2,c){ 
	      u2(x,y,z,c) = u1[wt](x,y,z,c); 
	      v2(x,y,z,c) = v1[wt](x,y,z,c); 
	    }
	  }
	}
      }
      // Limit the minimum possible variance
      v2.max(1e-9);
      // remove isolated pixels
      CImg<imap_pixel_t> imap2(imap, false);
      imap2.fill(0);
      CImg_3x3(I,imap_pixel_t);
      cimg_forZ(imap, z) cimg_for3x3(imap, x, y, z,0, I, imap_pixel_t){
	if (Icc != 0 && (Icp != 0 && Icn != 0 && Ipc != 0 && Inc != 0 )) imap2(x,y,z) = Icc;	
	else imap2(x,y,z) = 0;
      }
      imap = imap2;
    }
  }

  void Safir::display_data(const char * const title, const int ti)const{
#if cimg_display==1 || cimg_display==2
    CImg<unsigned char> visu(get_max_iter()*m_data->width(), 3*m_data->height(),
		m_data->depth(), m_data->spectrum(), 0);
    for (int l = 0; l < get_max_iter(); l++){
      visu.draw_image(l*m_data->width(), 0,
		      m_data->get_shared_estimate(ti,l).get_normalize(0,255));
      visu.draw_image(l*m_data->width(), m_data->height(),
		      m_data->get_shared_variance(ti,l).get_normalize(0,255));
      visu.draw_image(l*m_data->width(), 2*m_data->height(),
		      m_data->get_shared_iteration_map(ti).get_resize(-100,-100,-100,m_data->spectrum()).normalize(0,255));
    }
    static CImgDisplay disp(visu);
    disp.set_title(title);
    visu.display(disp);
#endif
  }

  void Safir::apply(){
    m_logger->info("ndsafir: bandwidth %f\n",
		   get_kernel_bandwidth((2*m_patch_width+1)*(2*m_patch_height+1)));
    if ((2*m_patch_width+1)*(2*m_patch_height+1)*(2*m_patch_depth+1)==1) {
      m_use_lookup=false;
      m_logger->info("ndsafir: disable lookup (patch too small)\n");
    }

    const float exp_threshold = -2.f * log(0.1f); // precompute a threshold for the gaussian

    for (int k = 1; k < get_max_iter(); k++){ // for each iteration

      const int  // compute the size of the seach zone
	wx = get_search_zone_width(k), wy = get_search_zone_height(k),
	wz = get_search_zone_depth(k), wt = get_search_zone_length(k);

      char label[2048]; // set-up a status label
      sprintf(label,"ndsafir %d:%dx%dx%dx%d",k,2*wx+1,2*wy+1,2*wz+1,2*wt+1);
      m_progress_bar->set_label(label);
      m_logger->infoline("ndsafir: step %d (%dx%dx%dx%d)",k,2*wx+1,2*wy+1,2*wz+1,2*wt+1);
      
      CImgList<> u0,v0,u1,v1,ub,vb; // init some list of images needed at each time point
      init_lists(u0,v0,u1,v1,ub,vb,k); // loading first frames [ | |X|X| ]

      for (int ti = 0; ti < m_data->length() && !m_progress_bar->is_canceled(); ti++) {	
		
	load_front_frame(u0,v0,u1,v1,ub,vb,k,ti); // loading front frame [ | | | |X]

	CImg<> // init the current estimates with zeros.
	  u2(m_data->get_shared_estimate(ti,k).fill(0), true),
	  v2(m_data->get_shared_variance(ti,k).fill(0), true),
	  sw(m_data->width(), m_data->height(), m_data->depth(), 1, 0);

	CImg<imap_pixel_t> imap(m_data->get_shared_iteration_map(ti), true); // load iteration map

	//#pragma omp parallel for collapse(3) // not supported by old gcc versions
	for (int zi=0;zi<u2.depth();zi+=m_sampling_depth){ // for each point in space
#pragma omp parallel for
	  cimg_for_stepY(u2,yi,m_sampling_height){
	    cimg_for_stepX(u2,xi,m_sampling_width){
	      float wmax = 0; // maximum weight in the search zone
	      const int tj0 = max(0, ti - wt), tj1 = min(m_data->length() - 1, ti + wt);
	      for (int tj = tj0; tj <= tj1; tj++){ // for each point in the seach zone
		cimg_forXYZ_window(u2, xi,yi,zi, xj,yj,zj, wx,wy,wz){
		  if (!(xi==xj && yi==yj && zi==zj && ti==tj) && imap(xi,yi,zi)==0){
		    // compute the distance, the weight and accumulate the patch for j to i.
		    const float d = distance_patch(u1,v1, ub,vb, xi,yi,zi,ti, xj,yj,zj,tj, k);		    
		    if (d < exp_threshold){
		      const float 
			dx = (float)(xi - xj) / (3.f * (float)wx),
			dy = (float)(yi - yj) / (3.f * (float)wy),
			dz = wz==0?0:(float)(zi - zj) / (3.f * (float)wz);
		      const float weight = exp(-0.5f * (d + dx*dx + dy*dy + dz*dz));
		      accumulate_patch(u2,v2,sw, u0,v0, xi,yi,zi,ti, xj,yj,zj,tj, weight,k);
		      wmax = max(wmax, weight); // compute max weight
		    }
		  }
		}
	      }
	      accumulate_patch(u2,v2,sw, u0,v0, xi,yi,zi,ti, xi,yi,zi,ti, wmax==0.0f?.5f:wmax, k); // central patch
	    }
	  }
	}
	normalize_step(u2, v2, sw, imap); // normalize (do not involve more info: can be done here)
	control_step(u2, v2, imap, u1, v1, ti, k); // control and apply wiener filtering
	roll_lists(u0, v0, u1, v1, ub, vb, k); // advance in time
	m_progress_bar->increase(); // update progress bar
	if (m_logger->get_verbosity() > 4) display_data("..", ti); // debug visualization
      }
    }
    m_logger->infoline("\n");
  }

}


#ifdef TEST_CSAFIR
#include "RAMMemoryManager.h"
#include "Log.h"
#include "ProgressBar.h"
#include "Data.h"

void test2D(void){
  const float noise_std=20;
  const int max_iter=5;
  CImg<> img("../data/lena.pgm");
  //  img.resize(300,200,1,1);
  CImg<> noisy(img.get_noise(noise_std));
  RAMMemoryManager *  data = new RAMMemoryManager;
  data->assign(img.width(),img.height(),img.depth(),img.spectrum(),1,max_iter);
  data->get_shared_estimate(0,0)=noisy;
  data->get_shared_variance(0,0)=noise_std*noise_std;  
  Log * logger = new Log;
  ProgressBar * progress_bar = new ProgressBar;
  Safir * safir = new Safir(data,logger,progress_bar);
  safir->set_patch_size(4,4,0);
  safir->set_lepski_threshold(4.);
  safir->set_kernel_pvalue(.1);
  cimg::tic();
  safir->apply();
  cimg::toc();
  CImg<> denoised=data->get_shared_estimate(0,data->number_of_iterations()-1);
  std::cout<<"PSNR:"<<img.PSNR(denoised)<<"dB"<<std::endl;
  float color[1]={255};
  img.get_append(noisy,'x')
    .get_append(denoised,'x')
    .draw_text(5,5,"psnr:%.2fdB",color,0,.7,17,img.PSNR(denoised))
    .display("Denoised",false);  
  delete data;
  delete logger;
  delete safir;
  delete progress_bar;
}

void testND(void){
  RAMMemoryManager * data = new RAMMemoryManager;
  data->assign(300,200,10,3,10,5);
  for (int t=0;t<data->length();t++){
    data->get_shared_estimate(t,0).noise(1.f);
    data->get_shared_variance(t,0)=1.f;
  }
  data->get_shared_estimate(0,0).display("Initial value");  
  Log * logger = new Log;
  ProgressBar * progress_bar = new ProgressBar;
  Safir * safir = new Safir(data,logger,progress_bar);
  safir->set_patch_size(1,1,1);
  safir->set_lepski_threshold(3.);
  safir->set_kernel_pvalue(.5);
  cimg::tic();
  safir->apply();
  cimg::toc();
  data->get_shared_estimate(0,data->number_of_iterations()-1).display("Final estimate");
  delete data;
  delete logger;
  delete safir;
  delete progress_bar;
}

int main(void){
  test2D();
  return 0;
}
#endif
