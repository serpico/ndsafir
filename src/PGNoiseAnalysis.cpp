// -*- mode:c++ -*-
/**
   \file PGNoiseAnalysis.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/
#include "PGNoiseAnalysis.h"
//#include "noise_analysis.h"
#include "cimgutils.h"
using namespace cimg;


namespace ndsafir{

  PGNoiseAnalysis::PGNoiseAnalysis(){m_flag=false;}

  PGNoiseAnalysis::PGNoiseAnalysis(Data * data, 
				   Log * logger,
				   ProgressBar * progress_bar):
    Element(data,logger,progress_bar)
  {  
    m_flag=false;
  }

  const char * PGNoiseAnalysis::get_name() const {
    return "PGNoiseAnalysis";
  }

  int PGNoiseAnalysis::get_number_of_steps()const{
    if (m_data)
      return 2*m_data->length();
    else
      return 0;
  }

  void PGNoiseAnalysis::apply(){
    if (!m_flag){
      estimate();
      //apply_gat();
      initialize_variance();
      m_flag=1;
    }
    else{
      //apply_uigat();
    }
  }

  void PGNoiseAnalysis::estimate(){
    m_progress_bar->set_label("Estimating Poisson-Gaussian noise...");
    m_g0.resize(m_data->spectrum(), m_data->length());
    m_edc.resize(m_data->spectrum(), m_data->length());
    for (int t = 0; t < m_data->length() && !m_progress_bar->is_canceled(); ++t){
      const CImg<> img(m_data->get_shared_estimate(t,0), false);
      cimg_forC(img, c){
	const CImg<> schannel(img.get_channel(c));
	double g0, edc, m, sigma;
	
	// DEBUT AJOUT CK 17/07/2019
	CImg<float> gat_params;
	gat_params = estimate_ccd_noise_parameters(schannel,3,false,false);
    g0 = (float)(gat_params[1]);
    edc = (float)(gat_params[0]); 	
	//printf("TGA parameters : gain g0 = %3.3f and Dark Current eDC = %3.3f \n",g0,edc);
	
	//schannel.gat_estimate_parameters(g0,edc,m,sigma,0,2,1,3,true,false);
	// FIN AJOUT  CK 17/07/2019
	
	if (g0 > 0){
	  m_g0(c,t) = g0; https://gitlab.inria.fr/serpico/deep-finder
	  m_edc(c,t) = edc;
	} else {
	  m_g0(c,t) = 1;
	  m_edc(c,t) = 0;
	}
      }
      m_progress_bar->increase();
    }
    m_g0.blur(1);
    m_edc.blur(1);
    if (m_data->length()==1 && m_data->spectrum()==1){
      m_logger->info("ndsafir: gain %f edc %f\n", m_g0(0), m_edc(0));
    } else {
      m_logger->info("ndsafir: gain %f [%.2f-%.2f] edc %f [%.2f-%.2f]\n",
		     m_g0.mean(), m_g0.min(), m_g0.max(),
		     m_edc.mean(), m_edc.min(), m_edc.max());
    }
  }

  void PGNoiseAnalysis::apply_gat(){
    for (int t=0;t<m_data->length() && !m_progress_bar->is_canceled();++t){
      CImg<> img(m_data->get_shared_estimate(t,0));
     // cimg_forC(img,c)
	//img.get_shared_channel(c).gat(m_g0(c,t),m_edc(c,t));
    } 
  }

  void PGNoiseAnalysis::apply_uigat(){
    const int last = m_data->number_of_iterations()-1;
    for (int t=0;t<m_data->length() && !m_progress_bar->is_canceled();++t){
      CImg<> img(m_data->get_shared_estimate(last,t),true);
     // cimg_forC(img,c)
	//img.get_shared_channel(c).uigat(m_g0(c,t),m_edc(c,t));
    }
  }

  void PGNoiseAnalysis::initialize_variance(){
    m_progress_bar->set_label("Initialize variance...");
    for (int t=0;t<m_data->length() && !m_progress_bar->is_canceled();++t){
      CImg<> v(m_data->get_shared_variance(t,0),true);
      CImg<> u(m_data->get_shared_estimate(t,0), false);
      const float ng=u.variance_noise();
      u.blur(2.f);
      cimg_forXYZC(u,x,y,z,c) v(x,y,z,c)=cimg::max(ng,m_g0(c,t)*u(x,y,z,c)+m_edc(c,t));
      m_progress_bar->increase();
    }
  }
}
#ifdef TEST_CSAFIRPGNOISEANALYSIS
#include "RAMMemoryManager.h"
#include "Log.h"
#include "ProgressBar.h"
#include "Data.h"

/*
int main(void){
  Log * logger = new Log;
  ProgressBar * progress_bar = new ProgressBar;
  RAMMemoryManager * data = new RAMMemoryManager;
  data->assign(200,200,3,1,10,1);   
  for (int t=0;t<data->length();t++){   
    data->get_shared_estimate(t,0).noise(1.f).blur(5).normalize(10,1000).noise(0,3);
  }
  data->get_shared_estimate(0,0).display();
  PGNoiseAnalysis * noise_analysis=new PGNoiseAnalysis(data,logger,progress_bar);
  noise_analysis->apply();
  printf("Noise variance is %f\n",data->get_shared_variance(0,0).mean());
  return 0;
}
*/

#endif
