// -*- mode:c++ -*-
/**
   \file Reader.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef READER_H
#define READER_H

#include "Element.h"
namespace ndsafir {
  //! Image Reader
  class Reader: public Element {
  public:
    Reader(Data * data, 
	   Log * logger,
	   ProgressBar * progress_bar);
    virtual void apply()=0;
    virtual const char * get_name() const;
    virtual int get_number_of_steps()const;
    virtual int width();
    virtual int height();
    virtual int depth();
    virtual int spectrum();
    virtual int length();
  protected:
    int m_width;
    int m_height;
    int m_depth;
    int m_spectrum;
    int m_length;
  };
}

#endif
