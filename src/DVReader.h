// -*- mode:c++ -*-
/**
   \file DVReader.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef DVREADER_H
#define DVREADER_H
#include <string>

#include "Reader.h"

namespace ndsafir {
  //! Delta Vision file reader
  /**
     To enable deltavision configure LDFLAGS and CFLAGS properly
  **/
  class DVReader: public Reader{
  public:
    //! Constructor
    DVReader(Data * data, 
             Log * logger,
             ProgressBar * progress_bar);
    //! Load the content of the file into the Data object
    virtual void apply();
    //! Get the name of the Element
    virtual const char * get_name() const;
    //! Get the number of steps used for loading
    int get_number_of_steps()const;
    //! Set the name of the file
    virtual void set_filename(std::string filename);
    //! Get the name of the file
    virtual std::string get_filename()const;
  protected:
    std::string m_filename;
  };
}
#endif
