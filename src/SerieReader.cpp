// -*- mode:c++ -*-
/**
   \file SerieReader.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#include "SerieReader.h"
#include "CImg.h"
using namespace cimg_library;
#include <fstream>
#include <iostream>

namespace ndsafir {

  SerieReader::SerieReader(Data * data, 
			   Log * logger,
			   ProgressBar * progress_bar):
    Reader(data,logger,progress_bar){
    m_one_file_per_time_point=true;
#ifdef cimg_use_tiff
    TIFFSetWarningHandler(0);
#endif
  }

  const char * SerieReader::get_name()const{
    return "SerieReader";
  }

  void SerieReader::set_one_file_per_time_point(const bool one_file_per_time_point){
    m_one_file_per_time_point=one_file_per_time_point;    
  }

  void SerieReader::set_from_txt(std::string filename){
    if (!m_one_file_per_time_point){
      m_logger->warn("ndsafir: SerieReader::set_from_txt() is uncompatible "
		     "when the mode one_file_per_time_point is not activated\n");
    }
    std::ifstream file(filename.c_str());
    clear();
    while(file.good()) {
      std::string str;
      getline(file,str);
      if (!str.empty()){
	std::ifstream ef(str.c_str()); // test if the file exists
	if (ef.is_open()){
	  m_filenames.push_back(str);	
	  ef.close();
	}
      }
    }
    file.close();
    if (!m_filenames.empty()){
      m_logger->info("ndsafir: got %d file from %s\n",m_filenames.size(),filename.c_str());
      const char * file_i=m_filenames[0].c_str();
      CImg<> img(file_i);
      m_width=img.width();
      m_height=img.height();
      m_depth=img.depth();
      m_spectrum=img.spectrum();
      m_length=m_filenames.size();
    }else{
      m_logger->error("ndsafir: could not load files from %s",filename.c_str());
    }
  }

  void SerieReader::set_filenames(std::vector<std::string> filenames){
    m_filenames=filenames;
    CImg<> img(m_filenames[0].c_str());
    m_width=img.width();
    m_height=img.height();
    if (m_one_file_per_time_point) {
      m_depth=img.depth();
      m_length=m_filenames.size();
    }
    else {
      m_depth=1;
      m_length=m_filenames.size()*img.depth();
    }
    m_spectrum=img.spectrum();
  }

  void SerieReader::add_filename(std::string filename){
    m_filenames.push_back(filename);  
    if (m_filenames.size()==1){
      CImg<> img(m_filenames[0].c_str());
      m_width=img.width();
      m_height=img.height();
      if (m_one_file_per_time_point) {
	m_depth=img.depth();
      } else {
	m_depth=1;
	m_length=img.depth();
      }
      m_spectrum=img.spectrum();
    }
    if (m_one_file_per_time_point) { 
      m_length=m_filenames.size();
    }
  }

  std::string SerieReader::get_filename()const{
    return m_filenames[0];
  }

  void SerieReader::clear(){
    m_filenames.clear();
  }

  void SerieReader::apply(){
    try{
      m_progress_bar->set_label("loading...");
      for (int t=0;t<(int)m_filenames.size();++t){
	if(m_filenames.size()>1) {
	  m_logger->infoline("ndsafir: loading %s",m_filenames[t].c_str());
	} else {
	  m_logger->info("ndsafir: loading %s\n",m_filenames[t].c_str());
	}
	if (m_one_file_per_time_point) {
	  m_data->get_shared_estimate(t,0).load(m_filenames[t].c_str());
	} else {
	  CImg<> img(m_filenames[t].c_str());
	  cimg_forZ(img, z)  m_data->get_shared_estimate(z, 0) = img.get_slice(z);	  
	}
	m_progress_bar->increase();
      }
      if(m_filenames.size()>1) m_logger->infoline("\n");
    }catch(CImgException e){
      m_logger->error("ndsafir: error while loading data w:%d h:%d d:%d l:%d s:%d\n\n",m_width,m_height,m_depth,m_length,m_spectrum);
      throw CImgException("Error in SerieReader::apply()");
    }
  }
}
