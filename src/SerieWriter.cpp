// -*- mode:c++ -*-
/**
   \file SerieWriter.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#include "SerieWriter.h"
#include "CImg.h"
using namespace cimg_library;
using namespace cimg;

#include <fstream>
#include <iostream>
#include <string>
namespace ndsafir {

  SerieWriter::SerieWriter(Data * data, 
			   Log * logger,
			   ProgressBar * progress_bar):
    Writer(data,logger,progress_bar){
    m_one_file_per_time_point = true;
#ifdef cimg_use_tiff
    TIFFSetWarningHandler(0);
#endif
  }

  const char * SerieWriter::get_name()const{
    return "SerieWriter";
  }

  int SerieWriter::get_number_of_steps()const{
    return m_filenames.size();
  }

  void SerieWriter::set_from_txt(std::string filename){
    std::ifstream file(filename.c_str());
    clear();
    while(file.good()) {
      std::string str;
      getline(file,str);
      //std::ifstream ef(str.c_str());
      //if (ef.is_open()){
      m_filenames.push_back(str);
	//ef.close();
	//}
    }
    file.close();
  }

  void SerieWriter::set_filenames(std::vector<std::string> filenames){
    m_filenames=filenames;
  }

  void SerieWriter::add_filename(std::string filename){
    m_filenames.push_back(filename);  
  }

  std::string SerieWriter::get_filename()const{
    if (m_filenames.empty()) {
      m_logger->warn("ndsafir: SerieWrite::get_filename() : filename not set.");
      return " ";
    }
    else return m_filenames[0];
  } 

  void SerieWriter::clear(){
    m_filenames.clear();
  }

  void SerieWriter::set_one_file_per_time_point(const bool one_file_per_time_point){
    m_one_file_per_time_point = one_file_per_time_point;
  }

  void SerieWriter::apply(){
    m_progress_bar->set_label("saving...");
    if (m_one_file_per_time_point) { // a file is equal to a time point
      const int nt = min((int)m_filenames.size(),m_data->length());
      for (int t=0;t<nt;++t){
	if(m_filenames.size()>1) {
	  m_logger->infoline("ndsafir: saving %s",m_filenames[t].c_str());
	} else {
	  m_logger->info("ndsafir: saving %s\n",m_filenames[t].c_str());
	}     
	m_data->get_shared_estimate(t,m_data->number_of_iterations()-1).save(m_filenames[t].c_str());
	m_progress_bar->increase();
      }
    } else { // will write the time serie into a single file
      if (!m_filenames.empty()){
	m_logger->info("ndsafir: saving %s\n", m_filenames[0].c_str());
	CImg<> img(m_data->width(), m_data->height(), m_data->length(), m_data->spectrum());
	for (int t = 0; t < m_data->length(); ++t){
	  CImg<> frame(m_data->get_shared_estimate(t, m_data->number_of_iterations() - 1));	  
	  for (int c = 0; c < m_data->spectrum(); ++c){
	    //img.get_shared_plane(t, c) = frame.get_shared_channel(c);
		img.get_shared_slice(t, c) = frame.get_shared_channel(c);
	  }
	  m_progress_bar->increase();
	}
	img.save(m_filenames[0].c_str());
      }
    }
  }

}
