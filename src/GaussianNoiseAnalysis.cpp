// -*- mode:c++ -*-
/**
   \file GaussianNoiseAnalysis.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#include "GaussianNoiseAnalysis.h"

namespace ndsafir{

  GaussianNoiseAnalysis::GaussianNoiseAnalysis(){}

  GaussianNoiseAnalysis::GaussianNoiseAnalysis(Data * data, 
					       Log * logger,
					       ProgressBar * progress_bar):
    Element(data,logger,progress_bar)
  {}

  const char * GaussianNoiseAnalysis::get_name()const{
    return "GaussianNoiseAnalysis";
  }

  int GaussianNoiseAnalysis::get_number_of_steps()const{
    if (m_data)
      return 2*m_data->length();
    else
      return 0;
  }

  void GaussianNoiseAnalysis::apply(){
    estimate();
    initialize_variance();
  }

  void GaussianNoiseAnalysis::estimate(){
    m_progress_bar->set_label("Estimating Gaussian noise...");
    m_noise_variance.resize(m_data->spectrum(),m_data->length());
    m_image_variance.resize(m_data->spectrum(),m_data->length());
    m_image_min.resize(m_data->spectrum(),m_data->length());
    m_image_max.resize(m_data->spectrum(),m_data->length());
    for (int t=0;t<m_data->length() && !m_progress_bar->is_canceled();++t){
      CImg<> img(m_data->get_shared_estimate(t,0));
      cimg_forC(img,c){
	CImg<> schannel(img.get_shared_channel(c));
	m_noise_variance(c,t)=schannel.variance_noise();
	CImg<> st(schannel.get_stats());
	m_image_variance(c,t)=st(3);
	m_image_min(c,t)=st(0);
	m_image_max(c,t)=st(1);
      }
      m_progress_bar->increase();
    }
    m_logger->info("ndsafir: noise std %.2f [%.2f-%.2f]\n", 
		   std::sqrt(m_noise_variance.mean()), 
		   std::sqrt(m_noise_variance.min()),
		   std::sqrt(m_noise_variance.max()));
  }
 
  void GaussianNoiseAnalysis::initialize_variance(){
    m_progress_bar->set_label("Initialize variance...");
    for (int t=0;t<m_data->length() && !m_progress_bar->is_canceled();++t){
      CImg<> img(m_data->get_shared_variance(t,0),true);
      cimg_forC(img,c) img.get_shared_channel(c).fill(m_noise_variance(c,t));
      m_progress_bar->increase();
    }
  }

  CImg<> GaussianNoiseAnalysis::get_psnr(){
    return (-20.f)*(((m_image_max-m_image_min).div(m_noise_variance)).log());
  }
}
#ifdef TEST_CSAFIRGAUSSIANNOISEANALYSIS
#include "RAMMemoryManager.h"
#include "Log.h"
#include "ProgressBar.h"
#include "Data.h"

template<typename T> inline void unused(const T&, ...){}

int main(void){
  Log * logger = new Log;
  ProgressBar * progress_bar = new ProgressBar;  
  RAMMemoryManager * data = new RAMMemoryManager;
  data->assign(10,10,10,3,10,2);  
  progress_bar->set_number_of_steps(data->length());
  for (int t=0;t<data->length();t++){
    progress_bar->increase();
    data->get_shared_estimate(t,0).noise(1.f);
  }
  GaussianNoiseAnalysis * noise_analysis=new GaussianNoiseAnalysis(data,logger,progress_bar);
  noise_analysis->apply();
  printf("Noise variance is %f\n",data->get_shared_variance(0,0).mean());
  return 0;
}

#endif
