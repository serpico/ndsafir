// -*- mode:c++ -*-
/**
   \file DVWriter.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef DVWRITER_H
#define DVWRITER_H

#include "Writer.h"
#include <string>
namespace ndsafir {
  //! Writer for DeltaVision (.dv) files
  class DVWriter: public Writer {
  public:
    DVWriter(Data * data,
	     Log * logger,
	     ProgressBar * progress_bar);
    //! Save the image in a DeltaVision (.dv) file in float
    /**
       If an original filename is set, transfers the header.
    **/
    virtual void apply();
    virtual const char * get_name() const;
    virtual int get_number_of_steps()const;
    //! Set the 
    virtual void set_filename(std::string filename);
    virtual std::string get_filename()const;
    //! Set the name of the original from which to copy the header
    virtual void set_original_filename(std::string filanme);
  private:
    std::string m_filename;
    std::string m_original_filename;
  };
}
#endif
