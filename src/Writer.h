// -*- mode:c++ -*-
/**
   \file WRITER.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#ifndef WRITER_H
#define WRITER_H

#include "Element.h"

#include <vector>
using namespace std;
namespace ndsafir {
  //! Data writer
  class Writer: public Element {
  public:
    Writer(Data * data,
		 Log * logger,
		 ProgressBar * progress_bar);
    virtual void apply()=0;
    virtual const char * get_name() const;
    virtual int get_number_of_steps()const=0;
  };
}
#endif
