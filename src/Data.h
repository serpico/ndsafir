// -*- mode:c++ -*-
/**
   \file Data.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/
#ifndef DATA_H
#define DATA_H

#include "CImg.h"
using namespace cimg_library;
typedef float imap_pixel_t;

namespace ndsafir {
  //! Data set used in by Safir
  /**
     It contains the 5D data of the estimate, variance and iteration
     map corresponding to several iterations. The purpose of this class
     to to offer the possibility to adapt the implementation of the
     storage of the dataset as a function of the available hardware.

     This is highly unstable and shared memory principle should be
     discarded in the futur in order to be compatible with on-disk
     caching and RAM-GPU transfert.
  **/
  class Data {

    /** @name API
     *  This set of method defines the interface
     */
    //@{
  public:

    //! Estimate at a given iteration and time point.
    /**
       \param iteration : iteration index
       \param time_point : time point index
    **/
    virtual CImg<>  get_shared_estimate( const int time_point,const int iteration)=0;
  
    //! Variance at a given iteration and time point.
    /**
       \param iteration : iteration index
       \param time_point : time point index
    **/
    virtual CImg<> get_shared_variance(const int time_point,const int iteration)=0;

    //! Iteration map at a given iteration and time point.
    /**
       \param time_point : time point index
    **/
    virtual CImg<imap_pixel_t> get_shared_iteration_map(const int time_point)=0;


    //! Get the width of the data
    virtual int width() const = 0;

    //! Get the height of the data
    virtual int height() const = 0;

    //! Get the depth of the data
    virtual int depth() const = 0;

    //! Get the number of channels of the data
    virtual int spectrum() const = 0;

    //! Get the number of time points of the data
    virtual int length() const = 0;

    //! Get the number of iterations to allocate
    virtual int number_of_iterations()const = 0;

    //! Get the total size of the dataset to store in memory
    virtual size_t size()const = 0;

    //! Assign the size of the dataset
    virtual void assign(int width, int height, int depth, int spectrum, int length, int iterations)=0;
 
    //@}
  };
}
#endif
