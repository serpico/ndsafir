// -*- mode:c++ -*-
/**
   \file Element.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#include "Element.h"

namespace ndsafir {

  Element::Element():
    m_logger(0),
    m_progress_bar(0),
    m_data(0)  
  {}

  Element::Element(Data * data, 
		   Log * logger,
		   ProgressBar * progress_bar):
    m_logger(logger),
    m_progress_bar(progress_bar),
    m_data(data)  
  {}

  void Element::apply(){}

  const char * Element::get_name()const {
    return "Default";
  }

  int Element::get_number_of_steps()const{
    return 0;
  }

  void Element::set_logger(Log * logger){
    m_logger=logger;
  }

  void Element::set_progress_bar(ProgressBar * progress_bar){
    m_progress_bar=progress_bar;
  }

  void Element::set_data(Data * data){
    m_data=data;
  }

}
