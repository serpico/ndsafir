// -*- mode:c++ -*-
/**
   \file Element.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef ELEMENT_H
#define ELEMENT_H

#include "Data.h"
#include "Log.h"
#include "ProgressBar.h"

#include <iostream>
#define dout std::cout<<__FILE__<<": line "<<__LINE__<<":  in "<<__PRETTY_FUNCTION__<<": "

namespace ndsafir {
  //! Element of a pipeline
  /**
     Includes log, memory and progress bar
  **/
  class Element {
  public:
    //! Basic constructor
    Element();
    //! Constructor
    Element(Data * data, 
	       Log * logger,
	       ProgressBar * progress_bar);
    //! Apply the processing task
    virtual void apply();
    //! Get the name of the object
    virtual const char * get_name() const;
    //! Get the number of processing steps
    /**
       This can be used to initialize the progress bar. By default the
       number of steps is 0.
    **/
    virtual int get_number_of_steps()const;
    //! Set the logger
    virtual void set_logger(Log* logger);
    //! Set the progress bar
    virtual void set_progress_bar(ProgressBar * progress_bar);
    //! Set the data set in which the task applies
    virtual void set_data(Data * data);
  protected:
    //! A log system
    /**
       \note The pointer is not owned.
    **/
    Log  * m_logger;
    //! A progress Bar
    /**
       \note The pointer is not owned.
    **/
    ProgressBar * m_progress_bar;
    //! A memory manager
    /**
       \note The pointer is not owned.
    **/
    Data * m_data;
  };
}
#endif
