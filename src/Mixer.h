// -*- mode:c++ -*-
/**
   \file Mixer.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef MIXER_H
#define MIXER_H

#include "Log.h"
#include "ProgressBar.h"
#include "Data.h"
#include "Element.h"
namespace ndsafir {
  //! Remix between input and output
  class Mixer : public Element {
  public:
    Mixer();
    Mixer(Data * data, 
		 Log * logger,
		 ProgressBar * progress_bar);
    virtual const char * get_name()const;
    virtual void apply();
    virtual int get_number_of_steps()const;
    virtual void set_value(const float value);
  protected:
    float m_value;
  };
}
#endif
