// -*- mode:c++ -*-
/**
   \file Log.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#include "Log.h"
#include <cstdarg>

namespace ndsafir {

Log::Log(){
    m_log_file=stdout;
    set_verbosity(0);
  }

  Log::~Log(){
    if (m_log_file!=stdout && m_log_file!=stderr) delete m_log_file;
  }

  void Log::set_verbosity(int level) {
    m_verbosity=level;
  }

  int Log::get_verbosity() const {
    return m_verbosity;
  }
  void Log::set_log_file(std::FILE * log_file){
    m_log_file=log_file;
  }

  void Log::error(const char *format,...) {
    if(m_log_file && m_verbosity>0){
      std::fprintf(m_log_file,"E:");
      std::va_list ap;
      va_start(ap,format);        
      std::vfprintf(m_log_file,format,ap);        
      va_end(ap);
      fflush(m_log_file);      
    }
  }

  void Log::warn(const char *format,...) {
    if(m_log_file && m_verbosity>1){
      std::fprintf(m_log_file,"W:");
      std::va_list ap;
      va_start(ap,format);        
      std::vfprintf(m_log_file,format,ap);        
      va_end(ap);
      fflush(m_log_file);      
    }
  }

  void Log::info(const char *format,...) {
    if(m_log_file && m_verbosity>2){
      std::va_list ap;                        
      va_start(ap,format);
      std::vfprintf(m_log_file,format,ap);        
      va_end(ap);
      fflush(m_log_file);
    }
  }

  void Log::infoline(const char *format,...) {
    if(m_log_file && m_verbosity>3){
      std::va_list ap;                    
      std::fprintf(m_log_file,"\r");
      va_start(ap,format);
      std::vfprintf(m_log_file,format,ap);        
      va_end(ap);
      fflush(m_log_file);
    }
  }
}

#ifdef TEST_CLOG
#include "stdio.h"
int main(void){
  Log * logger = new Log;
  logger->set_verbosity(2);
  logger->warn("Log::OK\n");
  delete logger;
  return 0;
}
#endif
