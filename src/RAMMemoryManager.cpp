// -*- mode:c++ -*-
/**
   \file RAMMemoryManager.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/
#include "RAMMemoryManager.h"

namespace ndsafir{

  RAMMemoryManager::RAMMemoryManager(){
    m_width    = 0;
    m_height   = 0;
    m_depth    = 0;    
    m_spectrum = 0;
    m_length   = 0; 
    m_number_of_iterations = 0;
    m_estimate = 0;
    m_variance = 0;
    m_iteration_map = 0;
  }

  RAMMemoryManager::~RAMMemoryManager(){
    if (m_estimate) delete[] m_estimate;
    if (m_variance) delete[] m_variance;
    if (m_iteration_map) delete[] m_iteration_map;
  }

  void RAMMemoryManager::assign(int width, int height, int depth, int spectrum, int length, int iterations){
    m_width    = width;
    m_height   = height; 
    m_depth    = depth; 
    m_spectrum = spectrum;
    m_length   = length; 
    m_number_of_iterations = iterations;
    size_t 
      sz0 = m_width*m_height*m_depth*m_spectrum*m_length*m_number_of_iterations,
      sz1 = m_width*m_height*m_depth*m_length;  
    if (m_estimate) delete[] m_estimate;
    if (m_variance) delete[] m_variance;
    if (m_iteration_map) delete[] m_iteration_map;
    m_estimate      = new float[sz0];
    m_variance      = new float[sz0];
    m_iteration_map = new imap_pixel_t[sz1];
    memset(m_estimate,0,sz0*sizeof(float));
    memset(m_variance,0,sz0*sizeof(float));
    memset(m_iteration_map,0,sz1*sizeof(imap_pixel_t));
  }

  CImg<> RAMMemoryManager::get_shared_estimate(const int time_point, const int iteration){
    float * ptr = m_estimate+m_width*m_height*m_depth*m_spectrum*(time_point+m_length*iteration);
    return CImg<>(ptr,m_width,m_height,m_depth,m_spectrum,true);
  }

  CImg<> RAMMemoryManager::get_shared_variance(const int time_point,const int iteration){
    float * ptr = m_variance+m_width*m_height*m_depth*m_spectrum*(time_point+m_length*iteration);
    return CImg<>(ptr,m_width,m_height,m_depth,m_spectrum,true);
  }

  CImg<imap_pixel_t> RAMMemoryManager::get_shared_iteration_map(const int time_point){
    imap_pixel_t * ptr = m_iteration_map+m_width*m_height*m_depth*time_point;
    return CImg<imap_pixel_t>(ptr,m_width,m_height,m_depth,1,true);
  }

  int RAMMemoryManager::width()const{return m_width;}
  int RAMMemoryManager::height()const{return m_height;}
  int RAMMemoryManager::depth()const{return m_depth;}
  int RAMMemoryManager::spectrum()const{return m_spectrum;}
  int RAMMemoryManager::length()const{return m_length;}
  int RAMMemoryManager::number_of_iterations()const{return m_number_of_iterations;}
  size_t RAMMemoryManager::size()const{return m_width*m_height*m_depth*m_length*(1+2*m_spectrum*m_number_of_iterations);}

}

#ifdef TEST_CSAFIRRAMMEMORYMANAGER
//g++ RAMMemoryManager.cpp  -o RAMMemoryManager -DTEST_CSAFIRRAMMEMORYMANAGER -lX11 -lpthread  -g -Wall
void test_size_alloc(){
  RAMMemoryManager * data=new RAMMemoryManager;
  data->assign(10,10,10,1,10,2); 
  CImg<> u1(data->get_shared_estimate(0,0).noise(10));
  CImg<> u2(data->get_shared_estimate(0,0));
  if (u1==u2 && data->width()==10 && data->height()==10&& data->depth()==10
      && data->spectrum()==1 && data->length()==10 
      && data->number_of_iterations()==2) printf("RAMMemoryManager::OK\n");
  else {
    printf("RAMMemoryManager::FAILED( x:%d y:%d z:%d c:%d l:%d n:%d)\n", data->width(),
           data->height(),data->depth(),data->spectrum(),
           data->length(),data->number_of_iterations());
  }
  delete data;
}

void test_content(){
  RAMMemoryManager * data=new RAMMemoryManager;
  data->assign(512,512,1,1,1,6);

  for (int t=0;t<data->length();t++){
    for (int i=0;i<data->number_of_iterations();i++){
      data->get_shared_estimate(t,i)=i*t;
      data->get_shared_variance(t,i)=i*t*2;
    }
    CImg<> imap(data->get_shared_iteration_map(t),true);
    for(int y=10;y<20;y++) for(int x=10;x<20;x++) imap(x,y)=1;
  }
  bool flag=1;
  for (int t=0;t<data->length();t++){

    for (int i=0;i<data->number_of_iterations();i++){
      flag=flag && (data->get_shared_estimate(t,i)==i*t);
      flag=flag && (data->get_shared_variance(t,i)==i*t*2);
    }
    CImg<imap_pixel_t> imap(data->get_shared_iteration_map(t));
    for(int y=10;y<20;y++) for(int x=10;x<20;x++) flag=flag && (imap(x,y)==1);
  }

  if (flag) printf("RAMMemoryManager::OK\n");
  else printf("RAMMemoryManager::FAILED (test_content)\n");
    
  delete data;
}

int main(void){
  test_content();
  return 0;
}
#endif
