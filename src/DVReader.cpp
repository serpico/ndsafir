// -*- mode:c++ -*-
/**
   \file DVReader.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#include "DVReader.h"
#ifdef cimg_use_ive
#include <IMInclude.h>
#endif
#include "CImg.h"
using namespace cimg_library;

namespace ndsafir {

  DVReader::DVReader(Data * data, 
		     Log * logger,
		     ProgressBar * progress_bar):
    Reader(data,logger,progress_bar)
  {}

  const char * DVReader::get_name() const{
    return "DVReader";
  }

  int DVReader::get_number_of_steps()const{
    return  m_length;
  }

  void DVReader::set_filename(std::string filename){
    m_filename=filename;
#ifdef cimg_use_ive
    IMAlPrt(false);
    int  istream_no=1;
    if (IMOpen(istream_no, filename.c_str(), "ro")) 
      throw CImgException("File %s does not exist.",filename.c_str());
    int nz,nt,nw,img_sequence;
    int ixyz[3], mxyz[3], pixeltype;
    float min, max, mean;
    IMRdHdr(istream_no, ixyz, mxyz, &pixeltype, &min, &max, &mean);
    IMRtZWT(istream_no, &nz, &nw, &nt, &img_sequence);
    m_width = ixyz[0];
    m_height = ixyz[1];
    m_depth = nz;
    m_spectrum = nw;
    m_length = nt;
#endif
  }

  std::string DVReader::get_filename()const{
    return m_filename;
  }

  void DVReader::apply(){
    m_progress_bar->set_label("ndsafir: loading");
#ifdef cimg_use_ive
    IMAlPrt(false);
    int istream_no = 1;
    if (IMOpen(istream_no, m_filename.c_str(), "ro")) 
      throw CImgException("File %s does not exist.",m_filename.c_str());
    int nx,ny,nz,nt,nw,nxy,nxyz,img_sequence;
    int ixyz[3], mxyz[3], pixeltype;
    float min, max, mean;
    IMRdHdr(istream_no, ixyz, mxyz, &pixeltype, &min, &max, &mean);
    IMRtZWT(istream_no, &nz, &nw, &nt, &img_sequence);
    nx = ixyz[0];
    ny = ixyz[1];
    nxy = nx * ny;
    nxyz = nxy * nz;  
    for (int ti=0;ti<nt;ti++){
      float * buffer =  m_data->get_shared_estimate(ti,0).data();  
      for (int wi=0; wi<nw; wi++)
	for (int zi=0; zi<nz; zi++){
	  IMPosnZWT(istream_no, zi, wi, ti);      
	  IMRdSec(istream_no, buffer+wi*nxyz+zi*nxy);
	}
      m_progress_bar->increase();
    }
    IMClose(istream_no);
#else
    m_logger->error("ndsafir: unable to open dv files.\n");
#endif
  }

}
