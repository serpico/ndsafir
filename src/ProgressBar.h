// -*- mode:c++ -*-
/**
   \file ProgressBar.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef CPROGRESSBAR_H
#define CPROGRESSBAR_H

#include "CImg.h"
using namespace cimg_library;

#include <string>
using namespace std;

namespace ndsafir {

  //! A class implementing a progress bar
  /**
     Keep track of the progess of a task. It also contains a signal
     system that allows to cancel a process from another task. This
     other task being also able to display progress information.
  **/
  class ProgressBar{
    /** @name API
     *  This set of method defines the interface
     */
    //@{
  public:
    //! Default constructor
    ProgressBar();
    //! Set number of steps
    virtual void set_number_of_steps(int n);

    //! Increase the progress bar
    virtual int increase();

    //! Decrease the progress bar
    virtual int decrease();

    //! render
    virtual void render();
  
    //! Cancel the process
    /**
       Usually the cancelling should be done through the signal system.
    **/
    virtual void cancel();

    //! Tell if the process is canceled
    virtual bool is_canceled();

    //! return the progress level as a float value between 0 and 1.
    virtual float get_progress();

    //! Set the text in the progress bar
    virtual void set_label(const string label);

    //! Set the title of the progress bar
    virtual void set_title(const string title);
    //@}

  
  protected:
    //! Number of steps in the process
    int m_number_of_steps;
    //! Index of the step in the process
    int m_index;
    //! Indicate if the process is being canceled
    bool m_is_canceled;
    //! Some text label
    string m_label;
    //! A title
    string m_title;
#if cimg_display==1 || cimg_display==2
    //! the window display
    CImgDisplay m_disp;
    //! the content of the window
    CImg<unsigned char> m_img;
#endif
  };
}
#endif
