// -*- mode:c++ -*-
/**
   \file GaussianNoiseAnalysis.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef GAUSSIANNOISEANALYSIS_H
#define GAUSSIANNOISEANALYSIS_H

#include "Data.h"
#include "Log.h"
#include "ProgressBar.h"
#include "Element.h"

#include "CImg.h"
using namespace cimg_library;
namespace ndsafir {
  //! Gaussian noise analysis and variance intialization
  class GaussianNoiseAnalysis : public  Element {
  public:
    GaussianNoiseAnalysis();
    GaussianNoiseAnalysis(Data * data, 
				Log * logger,
				ProgressBar * progress_bar);
    virtual const char * get_name()const;
    virtual int get_number_of_steps()const;
    virtual void apply();
    //! Computes Gaussian noise variance and image min/max
    void estimate();
    void initialize_variance();
    CImg<> get_psnr();
  protected:  
    CImg<> m_noise_variance;
    CImg<> m_image_variance;
    CImg<> m_image_min;
    CImg<> m_image_max;
  };
}
#endif
