// -*- mode:c++ -*-
/**
   \file Reader.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#include "Reader.h"

#include "CImg.h"
using namespace cimg_library;

namespace ndsafir {

  Reader::Reader(Data * data,
		 Log * logger,
		 ProgressBar * progress_bar):
    Element(data,logger,progress_bar)
  {}

  const char * Reader::get_name() const{
    return "Reader";
  }

  int Reader::get_number_of_steps()const{
    return  m_length;
  }

  int Reader::width(){
    return m_width;
  }
  int Reader::height(){
    return m_height;
  }

  int Reader::depth(){
    return m_depth;
  }

  int Reader::spectrum(){
    return m_spectrum;
  }

  int Reader::length(){
    return m_length;
  }
}
