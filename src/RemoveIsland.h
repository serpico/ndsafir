// -*- mode:c++ -*-
/**
   \file NoiseAnalysis.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef REMOVEISLAND_H
#define REMOVEISLAND_H

#include "Log.h"
#include "ProgressBar.h"
#include "Data.h"
#include "Element.h"
namespace ndsafir {
  //! Hot pixel removal
  class RemoveIsland : public Element {
  public:
    RemoveIsland(Data * data, 
		       Log* logger,
		       ProgressBar * progress_bar);
    void set_threshold(const float threshold);
    void set_iteration(const int iteration);
    void set_to_last();
    void set_to_first();
    void apply_to_iteration(const int iteration);
    virtual void apply();
    virtual const char * get_name() const;
    virtual int get_number_of_steps()const;
  private:
    //! A threshold on the value
    float m_threshold;
    //! On which iteration to apply
    int m_iteration;
  };
}
#endif
