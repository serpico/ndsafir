// -*- mode:c++ -*-
/**
   \file ProgressBar.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#include "ProgressBar.h"

namespace ndsafir{

  ProgressBar::ProgressBar(){
    m_number_of_steps=0;
    m_index=0; 
    m_is_canceled=false; 
    m_label=" ";
#if cimg_display==1 || cimg_display==2
    m_img.assign(200,100,1,3,32);
    m_disp.assign(m_img);
    render();
#endif
  }

  void ProgressBar::set_number_of_steps(int n){
    m_number_of_steps=n;
    m_index=0;
  }


  int ProgressBar::increase(){
    if (m_index<m_number_of_steps) m_index++;
    render();
    return m_index;
  }

  int ProgressBar::decrease(){
    if (m_index>0) m_index--;
    return  m_index;
  }

  void ProgressBar::render(){
#if (cimg_display==1 || cimg_display==2)
    m_disp.set_title(m_title.c_str());
    if(m_disp.is_resized()) m_disp.resize(m_disp);
    m_img.resize(m_disp).fill(64);  
    int w=0.80*m_img.width(),h=15,
      x0=m_img.width()/2-w/2, y0=m_img.height()/2-h/2-h/2,
      x1=m_img.width()/2+w/2, y1=m_img.height()/2+h/2-h/2;
    unsigned char color0[3]={128,128,128},color1[3]={0,255,0};
    if (m_disp.mouse_x()>x0 && m_disp.mouse_x()<x1 && 
	m_disp.mouse_y()>y0 && m_disp.mouse_y()<y1){
      color1[0]=200;
      color1[1]=90;    
      if (m_disp.button()&1) cancel();
      if (is_canceled()) {
	color1[0]=255;
	color1[1]=0;
      }
    }
    m_img.draw_rectangle(x0,y0,x0+get_progress()*(x1-x0),y1,color1,.5f);
    m_img.blur(1);
    m_img.draw_rectangle(x0,y0,x1,y1,color0,.5f,~0U);
    m_img.draw_rectangle(x0+1,y0+1,x1-1,y1-1,color0,.25f,~0U);
    if (m_label.size()!=0){
      const unsigned char bg=64;
      CImg<unsigned char> txt(200,100,1,3,64);
      txt.draw_text(0,0,m_label.c_str(),color0,0,1,13);
      txt.autocrop(bg);
      if (!txt.is_empty())
	m_img.draw_image(m_img.width()/2-txt.width()/2,y1+10,txt);
    }
    if (!m_img.empty()) m_img.display(m_disp);
#endif
  }

  void ProgressBar::cancel(){
    m_is_canceled=true;    
  }

  bool ProgressBar::is_canceled(){
    return m_is_canceled;
  }

  float ProgressBar::get_progress(){
    return cimg::max(0,cimg::min(1,(float)m_index/(float)(m_number_of_steps)));
  }

  void ProgressBar::set_label(const string label){  
    m_label=label;
    render();
  }

  void ProgressBar::set_title(const string title){
    m_title=title;
    render();
  }

}

#ifdef TEST_CPROGRESSBAR
#include <cstdio>
#include <cstdarg>

int main(void){
  ProgressBar *progress_bar=new ProgressBar;
  progress_bar->set_number_of_steps(50);
  for (int i=0;i<50 && !progress_bar->is_canceled();i++) {
    char label[2028];
    progress_bar->increase();
    sprintf(label,"%d%%",(int)(progress_bar->get_progress()*100));
    progress_bar->set_label(label);
    cimg::wait(100);
  }
  fflush(stdout);
  if (progress_bar->get_progress()==1) printf("ProgressBar::OK\n");
  else printf("ProgressBar::FAILED\n");
  return 0;
}
#endif

