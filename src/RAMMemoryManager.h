// -*- mode:c++ -*-
/**
   \file RAMMemoryManager.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef RAMMEMORYMANAGER_H
#define RAMMEMORYMANAGER_H

#include "Data.h"

#include "CImg.h"
using namespace cimg_library;
namespace ndsafir {
  //! Storage implementation of the Data using RAM
  /**
     In this implementation, the data are stored in RAM and swapping may
     occure.
  **/
  class RAMMemoryManager : public Data {
  public:
    RAMMemoryManager();
    ~RAMMemoryManager();
    //! Estimate at a given iteration and time point.
    /**
       \param iteration : iteration index
       \param time_point : time point index
    **/
    virtual CImg<>  get_shared_estimate(const int time_point,const int iteration);
  
    //! Variance at a given iteration and time point.
    /**
       \param iteration : iteration index
       \param time_point : time point index
    **/
    virtual CImg<> get_shared_variance(const int time_point,const int iteration);

    //! Iteration map at a given iteration and time point.
    /**
       \param time_point : time point index
    **/
    virtual CImg<imap_pixel_t> get_shared_iteration_map(const int time_point);
  
    //! Get the width of the data
    virtual int width() const;

    //! Get the height of the data
    virtual int height() const;

    //! Get the depth of the data
    virtual int depth() const;

    //! Get the number of channels of the data
    virtual int spectrum() const;

    //! Get the number of time points of the data
    virtual int length() const;

    //! Get the number of iterations to allocate
    virtual int number_of_iterations()const;

    //! Get the total size of the dataset to store in memory
    virtual size_t size()const;
 
    virtual void assign(int width, int height, int depth, int spectrum, int length, int iterations);

  protected:
    //! dimension along x axis
    size_t m_width;
    //! dimension along y axis
    size_t m_height;
    //! dimension along z axis
    size_t m_depth;
    //! Number of channels/colors
    size_t m_spectrum;
    //! Number of frame
    size_t m_length;
    //! Number of iterations
    size_t m_number_of_iterations;
    //! Estimate data
    float * m_estimate;
    //! Variance data
    float * m_variance;
    //! iteration map data
    imap_pixel_t * m_iteration_map;
  };
}
#endif
