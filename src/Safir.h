// -*- mode:c++ -*-
/**
   \file .h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
   copyright(c) 2003-2006 by INRA-MIA Jouy-en-Josas and INRIA Rennes. All Rights Reserved. 
**/

#pragma once
#ifndef SAFIR_H
#define SAFIR_H

#include "Log.h"
#include "ProgressBar.h"
#include "Data.h"
#include "Element.h"


// Include the CImg library
#include "CImg.h"
using namespace cimg_library;

namespace ndsafir {

  //! 4D+time patch-based denoising
  /**
     Implements the folowing paper:  <a href="http://www.irisa.fr/vista/Papers/2009_TMI_Boulanger.pdf">J. Boulanger, Ch. Kervrann, P. Bouthemy, P. Elbau, J.-B. Sibarita, J. Salamero. Patch-based non-local functional for denoising fluorescence microscopy image sequences. IEEE Trans on Medical Imaging, 29(2): 442-454, 2010</a> <a href="http://www.ncbi.nlm.nih.gov/pubmed/19900849">PMID:19900849</a>
     
     Up to 5D (3D color image sequences) can be handled.
     
     Includes the patch prefiltering in order to reduce the
     computation times of the distance between patches (lookup).

  **/
  class Safir : public Element{
  public:
    Safir(Data * data, 
	  Log * logger,
	  ProgressBar * progress_bar);
    virtual const char * get_name()const;
    virtual int get_number_of_steps()const;
    //! Apply the filter
    virtual void apply();

    //! Set the patch size (radius)
    void set_patch_size(const int rx, const int ry, const int rz);

    //! Set the bandwidth of the kernel
    /**
       Set the bandwidth of the kernel as well as the quantile for the
       normal distribution. The inverse cumulative distrubution of the
       normal distribution is computed using an approximation of the erf
       function described in http://en.wikipedia.org/wiki/Error_function

       \param val is the significance level (typically 1e-2).
    **/
    void set_kernel_pvalue(const float val);

    //! Set the use of mean/variance prefiltering
    /**
    The use of a lookup table was first introduced in 
    
    <a href="http://www.ima.umn.edu/preprints/jun2005/2052.pdf">M. Mahmoudi and G. Sapiro, “Fast Image and Video Denoising via Non-local Means of Similar Neighborhoods,”  IEEE Signal Processing Letters, vol. 12, no. 12, pp. 839-842, Dec. 2005.</a>

    The implemented version here is based on a test on mean value and
    variance of the patches. This allows to significantly reduce the
    computation time.

    **/
    void set_use_lookup(const bool use_lookup);

    //! Set the bandwidth of the kernel
    /**
       Get the bandwidth for the kernel using a normal approximation for
       the Chi square test. \f$
       \sqrt{2d}-\sqrt{2n-1}\sim\mathcal{N}(0,1)\f$. This approches
       allows to adapt the bandwidth automatically to the number of
       points in the patch for a given significance level.

       \param n is the number of points in the patch     
    **/
    float get_kernel_bandwidth(const int n) const;

    //! Set the threshold for the stopping rule 
    /**
       \param val is the threshold typically (between 3 and 4).
    **/
    void set_lepski_threshold(float val);

    //! Get the radius of the width of the search zone at iteration k
    /**
       \param k : iteration number

       \note The search zone width grows as \f$2^k\f$ if the time
       dimention is 1 otherwise grows as \f$2^{\lceil k-1/2 \rceil
       }\f$.
    **/
    int get_search_zone_width(const int k)const;

    //! Get the radius of the height of the search zone at iteration k
    /**
       \param k : iteration number 

       \note The search zone width grows as \f$2^k\f$ if the time
       dimention is 1 otherwise grows as \f$2^{\lceil k-1/2 \rceil
       }\f$.
    **/
    int get_search_zone_height(const int k)const;

    //! Get the radius of the depth of the search zone at iteration k
    /**
       \param k : iteration number

       \note The search zone width grows as \f$2^k\f$ if the time
       dimention is 1 otherwise grows as \f$2^{\lceil k-1/2 \rceil} \f$.
    **/
    int get_search_zone_depth(const int k)const;

    //! Get the radius of the length in time of the search zone at iteration k
    /**
       \param k : iteration number

       \note The search zone length grows as \f$ \lfloor k/2 \rfloor \f$.
    **/
    int get_search_zone_length(const int k)const;

    //! Get the maximum number of iteration
    /**
       The maximum number of iterations is determined by the Data.

       \note this might evolve in the future
    **/
    int get_max_iter()const;

  protected:
    //! Compute the mean and variance of patches of the image
    /**
       Apply a local mean and variance filter of the size of the patch
       in order to pre-compute a lookup table. 

       \see set_use_lookup
     **/
    void compute_mean_variance(const CImg<> &img, CImg<> & mean, CImg<> & var, int k)const;

    //! Compute the distance between two patches.
    /**       
       \param u the list of estimates around time point ti
       \param v the list of variances around time point ti
       \param ub the list of mean lookup table around time point ti
       \param vb the list of variances lookup table around time point ti
       \param u0 the image source for the estimate
       \param v0 the image source for the variance
       \param xi the x-coordinate of the patch at point i
       \param yi the y-coordinate of the patch at point i
       \param zi the z-coordinate of the patch at point i
       \param ti the t-coordinate of the patch at point i
       \param xj the x-coordinate of the patch at point j
       \param yj the y-coordinate of the patch at point j
       \param zj the z-coordinate of the patch at point j
       \param tj the t-coordinate of the patch at point j

      \note The distance between patch centered in \f$x\f$ and \f$y\f$ is
      defined by: \f$ \int G(t) \frac{u_{k-1}(x+t) -
      u_{k-1}(y+t)}{Q_{k-1}(x+t,y+t)} \, dt \f$ where \f$ Q(x+t,y+t)=
      \frac{v_{k-1}(x+t)+v_{k-1}(y+t)}{v_{k-1}(x+t)v_{k-1}(y+t)}\f$.
    **/
     float distance_patch(const CImgList<float> &u,const CImgList<float>&v,
				const CImgList<> &ub,const CImgList<> &vb,
				const int xi,const int yi,const int zi,const int ti,
				const int xj,const int yj,const int zj,const int tj, 
				const int k)const;

    //! Accumulate a patch from location (xj,yj,zj,tj) at (xi,yi,zi,ti) with a weight.
    /**
       \param u1 the accumulator for the estimate
       \param u2 the accumulator for the variance
       \param sw the accumulator for the sum of the weights
       \param u0 the image source for the estimate
       \param v0 the image source for the variance
       \param xi the x-coordinate of the destination patch
       \param yi the y-coordinate of the destination patch
       \param zi the z-coordinate of the destination patch
       \param ti the t-coordinate of the destination patch
       \param xj the x-coordinate of the source patch
       \param yj the y-coordinate of the source patch
       \param zj the z-coordinate of the source patch
       \param tj the t-coordinate of the source patch
       \param weight the weight
     **/
     void accumulate_patch(CImg<float> & u1, CImg<float> & v1, CImg<float> & sw, 
				 const CImgList<float> & u0, const CImgList<float> & v0, 
				 const int xi,const int yi,const int zi,const int ti,
				 const int xj,const int yj,const int zj,const int tj,
				 const float weight, const int k)const;

    //! Initialize the list of cached volumes at time t0
    /**
      Filled up the first elements of the list of images necessary to
      be processed (eg [ | |X|X| ]).
    **/
    void init_lists(CImgList<> &u0,CImgList<> &v0,CImgList<> &u1,CImgList<> &v1, CImgList<> &ub,CImgList<> &vb,const int k)const;

    //! Load the front frame
    /**
       Load the front frame in the array representing the neighborhood (eg [ | | | |X]).
     **/
    void load_front_frame(CImgList<> &u0, CImgList<> &v0,                   
			  CImgList<> &u1, CImgList<> &v1, 
			  CImgList<> &ub, CImgList<> &vb,
			  const int k, const int ti)const; 

    //! Advances in time the cached images
    /**
      Shift all the images by one in the list [0|1|2|3|4]->[1|2|3|4|0].
    **/
    void roll_lists(CImgList<> &u0, CImgList<> &v0, 
		    CImgList<> &u1, CImgList<> &v1, 
		    CImgList<> &ub, CImgList<> &vb,
		    const int k)const;

    //! Normalization step
    /**
      In this step, the estimates is normalized by the sum of the weights :
      \f$ u_2 = u_2/sw \f$.
    **/
    void normalize_step(CImg<>&u2, CImg<>&v2, CImg<>&sw, CImg<imap_pixel_t>& imap)const;

    //! Control step
    /**
       Includes a Wiener filtering between two steps and check the
       estimates using the Lepskii rule.
       \f[ \forall k^\prime, \quad |\hat{u}_{i,k}-\hat{u}_{i,k^\prime}| < \eta \nu_{i,k^\prime} \f]
     
    **/
    void control_step(CImg<>&u2, CImg<>&v2, CImg<imap_pixel_t>&imap,
		      CImgList<> &u1,CImgList<> &v1,const int t,const int k)const;

    //! Display the dataset in the case of 2D data sets
    void display_data(const char * const title, const int ti)const;

  protected:
    //! The patch width
    int m_patch_width;
    //! The patch height
    int m_patch_height; 
    //! The patch depth
    int m_patch_depth;
    //! Sub-sampling factor of the block based-approach in the width direction
    int m_sampling_width;
    //! Sub-sampling factor of the block based-approach in the height direction
    int m_sampling_height; 
    //! Sub-sampling factor of the block based-approach in the depth direction
    int m_sampling_depth;
    //! Bandwidth of the kernel
    float m_kernel_pvalue;
    //! Quantile corresponding to the pvalue
    float m_kernel_quantile;
    //! Threshold of the stopping rule
    float m_lepski_threshold;
    //! Use or not the mean and variance of the patches
    bool m_use_lookup;
  };

}
#endif  // SAFIR_H
