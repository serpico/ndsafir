// -*- mode:c++ -*-
/**
   \file loop_def.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/
#pragma once
#ifndef LOOP_DEF_H
#define LOOP_DEF_H
// loop (xj,yj) in a neighborhood of (xi,yj) of size w
#define cimg_for_step1(bound,i,step) for (int i = 0; i<(int)(bound); i+=step)
#define cimg_for_stepX(img,x,step) cimg_for_step1((img)._width,x,step)
#define cimg_for_stepY(img,y,step) cimg_for_step1((img)._height,y,step)
#define cimg_for_stepZ(img,y,step) cimg_for_step1((img)._depth,z,step)
#define cimg_for_stepXY(img,x,y,step) cimg_for_stepY(img,y,step) cimg_for_stepX(img,x,step)
#define cimg_for_stepXYZ(img,x,y,step) cimg_for_stepZ(img,z,step) cimg_for_stepY(img,y,step) cimg_for_stepX(img,x,step)

// Make a point j (xj,yj) go through the pixel lying within a neighborhood of point i (xi,yi)
#define cimg_forXY_window(img,xi,yi,xj,yj,rx,ry)			\
for (int yi0=cimg::max(0,yi-ry), yi1=cimg::min(yi+ry,(int)img.height()-1), yj=yi0;yj<=yi1;++yj) \
for (int xi0=cimg::max(0,xi-rx), xi1=cimg::min(xi+rx,(int)img.width()-1), xj=xi0;xj<=xi1;++xj) 

// Make a two points i_ and j_ go through two neighborhood located around points i and j
// is equivalent to two interleaved cimg_forXY_window but with intersecting boundaries
#define cimg_forXY_patch(img,xi,yi,xj,yj,xi_,yi_,xj_,yj_,px,py)  \
 const int dxp=cimg::min(xi-cimg::max(xi-px,0),xj-cimg::max(xj-px,0));  \
 const int dxn=cimg::min(cimg::min(xi+px,(int)img.width()-1)-xi,cimg::min(xj+px,(int)img.width()-1)-xj); \
 const int dyp=cimg::min(yi-cimg::max(yi-py,0),yj-cimg::max(yj-py,0));	\
 const int dyn=cimg::min(cimg::min(yi+py,(int)img.height()-1)-yi,cimg::min(yj+py,(int)img.height()-1)-yj); \
 const int pxi0=xi-dxp, pxi1=xi+dxn, pyi0=yi-dyp, pyi1=yi+dyn;		\
 const int pxj0=xj-dxp, pxj1=xj+dxn, pyj0=yj-dyp, pyj1=yj+dyn;		\
 for (int yi_=pyi0,yj_=pyj0; yi_<=pyi1 && yj_<=pyj1; ++yi_, ++yj_)	\
   for (int xi_=pxi0,xj_=pxj0; xi_<=pxi1 && xj_<=pxj1; ++xi_, ++xj_)	\

#define cimg_forXYZ_window(img,xi,yi,zi,xj,yj,zj,rx,ry,rz)		                        \
for (int zi0=cimg::max(0,zi-rz), zi1=cimg::min(zi+rz,(int)img.depth()-1) , zj=zi0;zj<=zi1;++zj) \
for (int yi0=cimg::max(0,yi-ry), yi1=cimg::min(yi+ry,(int)img.height()-1), yj=yi0;yj<=yi1;++yj) \
for (int xi0=cimg::max(0,xi-rx), xi1=cimg::min(xi+rx,(int)img.width()-1) , xj=xi0;xj<=xi1;++xj) 

// loop for 2 patch of size p around (xi,yj) and (xj,yj)
#define cimg_forXYZ_patch(img,xi,yi,zi,xj,yj,zj,xi_,yi_,zi_,xj_,yj_,zj_,px,py,pz) \
 const int dxp=cimg::min(xi-cimg::max(xi-px,0),xj-cimg::max(xj-px,0));  \
 const int dxn=cimg::min(cimg::min(xi+px,(int)img.width()-1)-xi,cimg::min(xj+px,(int)img.width()-1)-xj); \
 const int dyp=cimg::min(yi-cimg::max(yi-py,0),yj-cimg::max(yj-py,0));	\
 const int dyn=cimg::min(cimg::min(yi+py,(int)img.height()-1)-yi,cimg::min(yj+py,(int)img.height()-1)-yj); \
 const int dzp=cimg::min(zi-cimg::max(zi-pz,0),zj-cimg::max(zj-pz,0));	\
 const int dzn=cimg::min(cimg::min(zi+pz,(int)img.depth()-1)-zi,cimg::min(zj+pz,(int)img.depth()-1)-zj); \
 const int pxi0=xi-dxp, pxi1=xi+dxn, pyi0=yi-dyp, pyi1=yi+dyn, pzi0=zi-dzp, pzi1=zi+dzn;		\
 const int pxj0=xj-dxp, pxj1=xj+dxn, pyj0=yj-dyp, pyj1=yj+dyn, pzj0=zj-dzp, pzj1=zj+dzn;		\
 for (int zi_=pzi0,zj_=pzj0; zi_<=pzi1 && zj_<=pzj1; ++zi_, ++zj_)	\
   for (int yi_=pyi0,yj_=pyj0; yi_<=pyi1 && yj_<=pyj1; ++yi_, ++yj_)	\
     for (int xi_=pxi0,xj_=pxj0; xi_<=pxi1 && xj_<=pxj1; ++xi_, ++xj_)	\

#endif
