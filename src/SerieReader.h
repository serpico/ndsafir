// -*- mode:c++ -*-
/**
   \file SerieReader.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef SERIEREADER_H
#define SERIEREADER_H

#include "Reader.h"
#include <vector>
#include <string>
namespace ndsafir {
  //! File reader for still and image sequence.
  class SerieReader:public Reader{
  public:
    SerieReader(Data * data, 
		Log * logger,
		ProgressBar * progress_bar);
    virtual void apply();
    virtual const char * get_name() const;
    //! set the one_file_per_time_point mode for load files
    /**
       \note this function should be called before the setting the file names.
     **/
    virtual void set_one_file_per_time_point(const bool one_file_per_time_point);
    virtual void set_filenames(std::vector<std::string> filenames);
    virtual void add_filename(std::string filename);
    virtual void set_from_txt(std::string filename);
    virtual std::string get_filename()const;
    virtual void clear();
  protected:
    //! The list of the filenames
    std::vector<std::string> m_filenames;
    //! A flag indicating if a multi-page file (eg TIFF) is 3D or a sequence
    bool m_one_file_per_time_point;
  };
}
#endif
