// -*- mode:c++ -*-
/**
   \file PGNoiseAnalysis.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef PGNOISEANALYSIS_H
#define PGNOISEANALYSIS_H

#include "Data.h"
#include "Log.h"
#include "ProgressBar.h"
#include "Element.h"

#include "CImg.h"
using namespace cimg_library;
namespace ndsafir {
  //! Poisson Gaussian noise analysis
  class PGNoiseAnalysis: public Element {
  public:
    PGNoiseAnalysis();
    PGNoiseAnalysis(Data * data, 
		    Log * logger,
		    ProgressBar * progress_bar);
    virtual const char * get_name() const;
    virtual int get_number_of_steps()const;
    //! Perform noise anaysis and variance stabilization
    /**
       If applied twice the second time, the inverse transform using the
       same parameters is performed.
    **/
    virtual void apply();
    //! Estimate the parameters
    void estimate();
    //! Initialize the variance
    void initialize_variance();
    //! Estimate a PSNR
    float get_psnr();
    //! Apply the Generalized Anscombe Transform
    void apply_gat();
    //! Apply the inverse Generalized Anscombe Transform
    void apply_uigat();
  protected:  
    CImg<> m_g0;
    CImg<> m_edc;
    bool m_flag;
  };
}
#endif

