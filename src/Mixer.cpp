// -*- mode:c++ -*-
/**
   \file Mixer.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/
#include "Mixer.h"
#include "CImg.h"
using namespace cimg_library;
using namespace cimg;

namespace ndsafir {

  Mixer::Mixer(){}

  Mixer::Mixer(Data * data, 
		 Log * logger,
		 ProgressBar * progress_bar):
    Element(data,logger,progress_bar)
  {
    m_value=1.0f;
  }

  const char * Mixer::get_name()const{
    return "Mixer";
  }

  int Mixer::get_number_of_steps()const{
    return 0;
  }

  void Mixer::set_value(const float value){
    m_value=max(0.f,min(1.f,value));
  }

  void Mixer::apply() {
    if (m_value<0.999f){
      m_progress_bar->set_label("applying mixer");
      const int last=m_data->number_of_iterations()-1;
      for (int t=0;t<m_data->length() && !m_progress_bar->is_canceled();t++){
	CImg<> u1=m_data->get_shared_estimate(t,0);
	CImg<> v1=m_data->get_shared_variance(t,0);
	CImg<> u2=m_data->get_shared_estimate(t,last);
	CImg<> v2=m_data->get_shared_variance(t,last);
	cimg_foroff(u2,i) {
	  u2(i)=(1-m_value)*u1(i)+m_value*u2(i);
	  v2(i)=(1-m_value)*v1(i)+m_value*v2(i);
	}
	m_progress_bar->increase();
      }
    }
  }

}

#ifdef TEST_CSAFIRWIENER
#include "RAMMemoryManager.h"
#include "Log.h"
#include "ProgressBar.h"
#include "Data.h"
int main(void){
  RAMMemoryManager * data = new RAMMemoryManager;
  data->assign(10,10,10,3,10,2);
  for (int i=0;i<data->number_of_iterations();i++)
    for (int t=0;t<data->length();t++){
      data->get_shared_estimate(t,i)=(float)(100*i);
      data->get_shared_variance(t,i)=1.f;
    }
  Log * logger = new Log;
  ProgressBar * progress_bar = new ProgressBar;
  Mixer * wiener = new Mixer(data,logger,progress_bar);
  wiener->apply();
  if (data->get_shared_estimate(0,1)(5,5,5)==50) printf("Mixer::OK\n");
  else {    
    printf("Mixer::FAILED\n");
    printf("value is :%f\n",data->get_shared_estimate(0,1)(5,5,5));
  }
  return 0;
}
#endif
