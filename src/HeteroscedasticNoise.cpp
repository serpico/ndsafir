// -*- mode:c++ -*-
/**
   \file HeteroscedasticNoise.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#include "HeteroscedasticNoise.h"


namespace ndsafir{

  HeteroscedasticNoise::HeteroscedasticNoise():m_factor(1.f){}

  HeteroscedasticNoise::HeteroscedasticNoise(Data * data, 
					     Log * logger,
					     ProgressBar * progress_bar):
    Element(data,logger,progress_bar)
  {
    m_factor=1.f;
  }

  const char * HeteroscedasticNoise::get_name()const{
    return "HeteroscedasticNoise";
  }

  int HeteroscedasticNoise::get_number_of_steps()const{
    if (m_data)
      return 2*m_data->length();
    else
      return 0;
  }
  
  void HeteroscedasticNoise::set_factor(const float factor){
    m_factor = factor;
  }

  void HeteroscedasticNoise::apply(){
    initialize_variance();
  }
  

  void HeteroscedasticNoise::initialize_variance(){
     m_progress_bar->set_label("Initialize variance...");
     const int h = 3, n = (2*h+1)*(2*h+1);
     CImg<> mx(2*h+1,1,1,1,1), my(1,2*h+1,1,1,1), mean, var;
     for (int t = 0; t < m_data->length() && !m_progress_bar->is_canceled(); ++t){
       const CImg<> img(m_data->get_shared_estimate(t, 0), true);
       const float ng = img.variance_noise();
       mean.assign(img, false).convolve(mx).convolve(my);
       var.assign(img.get_pow(2)).convolve(mx).convolve(my);
       mean /= n;   var /= n;   var -= mean.get_pow(2);
       var.blur(5).max(ng);
       if ( m_factor != 1.f) var *= m_factor;
       m_data->get_shared_variance(t,0) = var;
       m_progress_bar->increase();
     }
  }

  CImg<> HeteroscedasticNoise::get_psnr(){
    return CImg<>();
  }
}
