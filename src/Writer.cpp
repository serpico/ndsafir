// -*- mode:c++ -*-
/**
   \file Writer.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#include "Writer.h"
#include "CImg.h"
using namespace cimg_library;

namespace ndsafir {

  Writer::Writer(Data * data, 
		 Log * logger,
		 ProgressBar * progress_bar):
    Element(data,logger,progress_bar)
  {}


  const char * Writer::get_name()const{
    return "Writer";
  }

}
