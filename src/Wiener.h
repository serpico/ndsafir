// -*- mode:c++ -*-
/**
   \file Wiener.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#ifndef WIENER_H
#define WIENER_H

#include "Log.h"
#include "ProgressBar.h"
#include "Data.h"
#include "Element.h"
namespace ndsafir {
  //! Post wiener filtering
  class Wiener : public Element {
  public:
    Wiener();
    Wiener(Data * data, 
	   Log * logger,
	   ProgressBar * progress_bar);
    virtual const char * get_name()const;
    virtual void apply();
    virtual int get_number_of_steps()const;
  };
}
#endif
