// -*- mode:c++ -*-
/**
   \file DVWriter.cpp
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#include "DVWriter.h"
#ifdef cimg_use_ive
#include <IMInclude.h>
#endif
#include "CImg.h"
using namespace cimg_library;

namespace ndsafir {

  DVWriter::DVWriter(Data * data,
		     Log * logger,
		     ProgressBar * progress_bar):
    Writer(data,logger,progress_bar)
  {}


  const char * DVWriter::get_name()const{
    return "DVWriter";
  }

  int DVWriter::get_number_of_steps()const{
    return m_data->length();
  }

  void DVWriter::set_filename(std::string filename){
    m_filename=filename;
#ifndef cimg_use_ive
    m_logger->error("ndsafir: unable to save dv files.\n");
#endif
  }

  std::string DVWriter::get_filename()const{
    return m_filename;
  }

  void DVWriter::set_original_filename(std::string filename){
    m_original_filename=filename;
  }

  void DVWriter::apply(){
#ifdef cimg_use_ive
    IMAlPrt(false);
    int ostream_no = 2;
    if (IMOpen(ostream_no, m_filename.c_str(), "new")) 
      throw CImgException("DVWriter::apply() : cannot create file %s.",m_filename.c_str());
    if (!m_original_filename.empty()){
      int istream_no = 1;
      if (IMOpen(istream_no, m_original_filename.c_str(), "ro")) 
	throw CImgException("DVWriter::apply() : cannot open file %s.",m_original_filename.c_str());
      IMTrHdr(ostream_no, istream_no);
      IMClose(istream_no);
    }
    const int nxy= m_data->width()*m_data->height(),nxyz=nxy*m_data->depth();
    const int ixyz[3]={m_data->width(),m_data->height(),m_data->depth()}, mxyz[3]={1,1,1}, pixeltype=2;
    IMAlMode(ostream_no, IW_FLOAT); // save images in float
    IMAlZWT(ostream_no, m_data->depth(),m_data->spectrum(), 1, 2);  
    IMCrHdr(ostream_no, ixyz, mxyz, pixeltype, 0,0);
    float mini=1e30,maxi=-1e30,avg=0;
    for (int t=0;t<m_data->length();t++){   
      CImg<> tmp(m_data->get_shared_estimate(t,m_data->number_of_iterations()-1));
      mini=min(mini,(float)tmp.min());
      maxi=min(maxi,(float)tmp.max());
      avg+=tmp.mean();
      for (int wi=0; wi<m_data->spectrum(); wi++)
	for (int zi=0; zi<m_data->depth(); zi++) {
	  IMPosnZWT(ostream_no,zi,wi,0);
	  IMWrSec(ostream_no, tmp.data()+wi*nxyz+zi*nxy); 
	}
      m_progress_bar->increase();
    }
    avg/=m_data->length();
    IMWrHdr(ostream_no, "Denoised with ndsafir algorithm.", 1, mini, maxi, avg);
    IMClose(ostream_no);  
#else
    m_logger->error("ndsafir: unable to save dv files.\n");
#endif
  }
}
