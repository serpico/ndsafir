// -*- mode:c++ -*-
/**
   \file HeteroscedasticNoise.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef HETEROSCEDASTICNOISE_H
#define HETEROSCEDASTICNOISE_H

#include "Data.h"
#include "Log.h"
#include "ProgressBar.h"
#include "Element.h"

#include "CImg.h"
using namespace cimg_library;
namespace ndsafir {
  //! Gaussian noise analysis and variance intialization
  class HeteroscedasticNoise : public  Element {
  public:
    HeteroscedasticNoise();
    HeteroscedasticNoise(Data * data, 
			 Log * logger,
			 ProgressBar * progress_bar);
    virtual const char * get_name()const;
    virtual int get_number_of_steps()const;
    virtual void apply();
    void set_factor(const float factor);
    void initialize_variance();
    CImg<> get_psnr();
  protected:
    float m_factor;
  };
}
#endif
