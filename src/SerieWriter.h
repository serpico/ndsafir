// -*- mode:c++ -*-
/**
   \file SerieWriter.h
   \author Jerome Boulanger (jerome.boulanger@laposte.net)
**/

#pragma once
#ifndef SERIEWRITER_H
#define SERIEWRITER_H

#include "Writer.h"
#include <vector>
#include <string>

namespace ndsafir {
  //! Image file writer for image series (tiff,png,jpeg)
  class SerieWriter:public Writer {
  public:
    SerieWriter(Data * data, 
		      Log * logger,
		      ProgressBar * progress_bar);
    virtual void apply();
    virtual const char * get_name() const;
    virtual int get_number_of_steps()const;
    virtual void set_filenames(std::vector<std::string> filenames);
    virtual void add_filename(std::string filename);    
    virtual void set_one_file_per_time_point(const bool one_file_per_time_point);
    //! Set the file list from a text file
    virtual void set_from_txt(std::string filename);
    virtual std::string get_filename()const;
    virtual void clear();
  protected:
    //! List of file names
    std::vector<std::string> m_filenames;
    //! A flag indicating if a time point is associated to a file
    bool m_one_file_per_time_point;
  };
}
#endif
