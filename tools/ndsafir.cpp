// -*- mode:c++ -*-
/*
  \file  ndsafir.cpp
  \author Jerome Boulanger (jerome.boulanger@laposte.net)
*/

/*
#include "Safir.h"
#include "Data.h"
#include "Element.h"
#include "GaussianNoiseAnalysis.h"
#include "PGNoiseAnalysis.h"
#include "HeteroscedasticNoise.h"
#include "Wiener.h"
#include "RemoveIsland.h"
#include "RAMMemoryManager.h"
#include "Reader.h"
#include "Writer.h"
#include "DVReader.h"
#include "DVWriter.h"
#include "SerieReader.h"
#include "SerieWriter.h"
#include "Mixer.h"
*/
#include <ndsafir>
using namespace ndsafir;

#include "CImg.h"
using namespace cimg_library;
#include <vector>
#include <sstream>
#include <string>
#include <iostream>
#include <map>
#include <memory>
#include <ctime>
#include <algorithm>
using namespace std;

//! Command line user class for using ndsafir
/**
   This is a user class to manage the libndsafir and all its component
   in order to provide a command line interface. We basically first
   parse the command line, and build a pipeline and later run the
   pipeline. This allows for example to compute in a flexible way the
   number of processing steps and later to store pipleline in a queue
   for example.
**/
class Cli{
public:
  Cli():
    m_logger(new Log),
    m_progress_bar(new ProgressBar),
    m_data(new RAMMemoryManager){
    m_logger->set_log_file(stdout);
    m_progress_bar->set_title("ndsafir v3.0");
  }  

  ~Cli(){
    time(&m_endtime);
    m_logger->info("ndsafir: ending date %s\n",ctime (&m_endtime));
    vector<Element *>::iterator element;
    for (element=m_pipeline.begin();element!=m_pipeline.end();++element){      
      if (*element!=NULL) delete *element;
    }
    delete m_data;
    delete m_progress_bar;
    delete m_logger;
  }

  string get_format(const string filename){
    string ext=filename.substr(filename.rfind('.')+1,filename.size()-1);
    std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
    return ext;
  }

  std::string etime_str(){   
    const unsigned long dt = m_t1-m_t0;
    const unsigned int
      edays = (unsigned int)(dt/86400000.0),
      ehours = (unsigned int)((dt - edays*86400000.0)/3600000.0),
      emin = (unsigned int)((dt - edays*86400000.0 - ehours*3600000.0)/60000.0),
      esec = (unsigned int)((dt - edays*86400000.0 - ehours*3600000.0 - emin*60000.0)/1000.0),
      ems = (unsigned int)(dt - edays*86400000.0 - ehours*3600000.0 - emin*60000.0 - esec*1000.0);
    stringstream str;
    if (!edays && !ehours && !emin && !esec)
      str<<ems<<"ms";
    else {
      if (!edays && !ehours && !emin)
        str<<esec<<"s "<<ems<<"ms";          
      else {
        if (!edays && !ehours)
          str<<emin<<"min "<<esec<<"s "<<ems<<"ms";
        else{
          if (!edays)
            str<<ehours<<"h "<<emin<<"min "<<esec<<"s "<<ems<<"ms";
          else{
            str<<edays<<"days "<<ehours<<"h "<<emin<<"min "<<esec<<"s "<<ems<<"ms";
          }
        }
      }
    }
    return str.str();    
  }

  //! Parse the command arguments and build a pipeline
  void parse0(int argc, char *argv[]){
    cimg_usage("ndsafir");
    const char * file_i =          cimg_option("-i"       ,(char*)0,"input (list of files as .txt)");
    const char * file_o =          cimg_option("-o"       ,(char*)0,"output (list of files as .txt)");
    const bool   is_2dt =          cimg_option("-2dt"     ,false   ,"consider single file as time series");
    const int    max_iter  =       cimg_option("-iter"    ,5       ,"iterations");
    const char * patch_str =       cimg_option("-patch"   ,"7x7x1" ,"patch size");
    const double kernel_pvalue =   cimg_option("-pval"    ,1.0e-7  ,"kernel pvalue");
    const double lepski_threshold =cimg_option("-eta"     ,4.0     ,"Stopping threshold");
    const double use_lookup =      cimg_option("-lut"     ,true    ,"use a lut to prefilter");
    const int    noise_type =      cimg_option("-noise"   ,1       ,"noise (0:Gauss.  1:Poisson-Gauss. 2:Adaptative-Gauss.)");   
    const bool   wiener =          cimg_option("-wiener"  ,false   ,"post wiener filt.");
    const float  mixer =           cimg_option("-mix"     ,1.0     ,"mix denoised and noisy version");
    const float  pre_rhp =         cimg_option("-rhp0"    ,-1      ,"pre hot pix filt. (approx. 9)");
    const float  post_rhp =        cimg_option("-rhp1"    ,-1      ,"post hot pix filt. (approx. 9)");
    //const int    num_threads =     cimg_option("-nthreads",omp_get_max_threads(),"max number of threads (<1:auto)");
    const int    verbosity =       cimg_option("-v"       ,1       ,"verbosity level");    
    const char * logfilename =     cimg_option("-log"     ,(char*)0,"log file");
    const bool   ndsafir =         cimg_option("-ndsafir" ,true    ,"apply ndsafir");
    const float  noise_factor  =    cimg_option("-nf"     ,(float)1.f     ,"noise factor");

    try{      
      if (verbosity>4) cimg::info();
      if (logfilename){
        FILE * logfile=fopen(logfilename,"a");
        m_logger->set_log_file(logfile);
      }
      m_logger->set_verbosity(verbosity);
      m_logger->info("ndsafir: version 3 (%s %s)\n",__DATE__,__TIME__);
      time(&m_starttime);
      m_t0=cimg::time();
      m_logger->info("ndsafir: starting date %s",ctime (&m_starttime));
   
      //if (num_threads>0) omp_set_num_threads(num_threads);
      //else omp_set_num_threads(omp_get_max_threads());
      //m_logger->info("ndsafir: using %d threads at most\n",omp_get_max_threads());

      // Build the pipeline
      m_pipeline.clear();

      // Add reader
      if (file_i){
	if (get_format(file_i)=="dv"){
	  m_pipeline.push_back(new DVReader(m_data,m_logger,m_progress_bar));
	  DVReader * reader(dynamic_cast<DVReader*>(m_pipeline[m_pipeline.size()-1]));
	  reader->set_filename(file_i);
	  m_logger->info("ndsafir: set input to %s\n",reader->get_filename().c_str());
	  m_data->assign(reader->width(),reader->height(),reader->depth(),
			 reader->spectrum(),reader->length(),max_iter);	
	}else{
	  m_pipeline.push_back(new SerieReader(m_data,m_logger,m_progress_bar));
	  SerieReader *reader(dynamic_cast<SerieReader*>(m_pipeline[m_pipeline.size()-1]));
	  if (get_format(file_i)=="txt"){
	    reader->set_one_file_per_time_point(true);
	    reader->set_from_txt(file_i);
	  }else{
	    if (is_2dt) reader->set_one_file_per_time_point(false);
	    reader->add_filename(file_i);
	  }
	  m_logger->info("ndsafir: set input to %s\n",reader->get_filename().c_str());
	  m_data->assign(reader->width(),reader->height(),reader->depth(),
			 reader->spectrum(),reader->length(),ndsafir?max_iter:1);
	}
      }else{
	m_logger->error("ndsafir: Input (option -i) needed\n");
	exit(1);
      }

      print_data_size();
     

      // Add noise analysis
      switch (noise_type){
      case 0:
	m_logger->info("ndsafir: gaussian noise analysis selected\n");
	m_pipeline.push_back(new GaussianNoiseAnalysis(m_data,m_logger,m_progress_bar));
	break;
      case 1:
	m_logger->info("ndsafir: poisson-gaussian noise analysis selected\n");
	m_pipeline.push_back(new PGNoiseAnalysis(m_data,m_logger,m_progress_bar));
	break;
      case 2:
	m_logger->info("ndsafir: heteroscedastic noise analysis selected\n");
	m_pipeline.push_back(new HeteroscedasticNoise(m_data,m_logger,m_progress_bar));
	dynamic_cast<HeteroscedasticNoise*>(m_pipeline[m_pipeline.size()-1])->set_factor(noise_factor);
	break;
      }

      // Add remove hot pixel
      if (pre_rhp>0){
	m_logger->info("ndsafir: hot pixel pre-filtering activated\n");
	m_pipeline.push_back(new RemoveIsland(m_data,m_logger,m_progress_bar));
	dynamic_cast<RemoveIsland*>(m_pipeline[m_pipeline.size()-1])->set_to_first();
	dynamic_cast<RemoveIsland*>(m_pipeline[m_pipeline.size()-1])->set_threshold(pre_rhp);
      }

      // Add ndsafir filter
      if (ndsafir){
	m_pipeline.push_back(new Safir(m_data,m_logger,m_progress_bar));
	Safir * safir(dynamic_cast<Safir*>(m_pipeline[m_pipeline.size()-1]));
	int rx=7,ry=7,rz=1;
	sscanf(patch_str,"%dx%dx%d",&rx,&ry,&rz);
	safir->set_patch_size(rx/2,ry/2,rz/2);
	safir->set_lepski_threshold(lepski_threshold);
	safir->set_kernel_pvalue(kernel_pvalue);
	safir->set_use_lookup(use_lookup);
      }

      // Add Wiener filter
      if (wiener){
	m_logger->info("ndsafir: wiener post-processing activated\n");
	m_pipeline.push_back(new Wiener(m_data,m_logger,m_progress_bar));
      }

      // Add the post hot pixel removal
      if (post_rhp>0){
	m_logger->info("ndsafir: hot pixel post-filtering activated\n");
	m_pipeline.push_back(new RemoveIsland(m_data,m_logger,m_progress_bar));
	dynamic_cast<RemoveIsland*>(m_pipeline[m_pipeline.size()-1])->set_to_last();
	dynamic_cast<RemoveIsland*>(m_pipeline[m_pipeline.size()-1])->set_threshold(post_rhp);
      }

      // Add mixer
      if (mixer<0.999f){
	m_logger->info("ndsafir: mixer (%.3f) activated \n",mixer);
	m_pipeline.push_back(new Mixer(m_data,m_logger,m_progress_bar));
	dynamic_cast<Mixer*>(m_pipeline[m_pipeline.size()-1])->set_value(mixer);
      }

      // Add the writer
      if (file_o){
	if (get_format(file_o)=="dv"){
	  m_pipeline.push_back(new DVWriter(m_data,m_logger,m_progress_bar));
	  DVWriter * saver(dynamic_cast<DVWriter*>(m_pipeline[m_pipeline.size()-1]));
	  saver->set_filename(file_o);
	  m_logger->info("ndsafir: set output to %s\n",saver->get_filename().c_str());
	}else{
	  m_pipeline.push_back(new SerieWriter(m_data,m_logger,m_progress_bar));
	  SerieWriter *writer(dynamic_cast<SerieWriter*>(m_pipeline[m_pipeline.size()-1]));
	  if (get_format(file_o)=="txt"){
	    writer->set_one_file_per_time_point(true);
	    writer->set_from_txt(file_o);
	  }else{
	    if (is_2dt) writer->set_one_file_per_time_point(false);
	    writer->add_filename(file_o);
	  }
	  m_logger->info("ndsafir: set output to %s\n",writer->get_filename().c_str());
	}
      }
    }catch(CImgException e){
      m_logger->error("ndsafir: error occured while parsing\n");
      exit(1);
    }
  }
    
  //! Print the dataset size
  void print_data_size(){
    if (m_data){
      size_t 
        sz0 = m_data->width()*m_data->height()*m_data->depth()*m_data->spectrum()*m_data->length()*m_data->number_of_iterations(),
        sz1 = m_data->width()*m_data->height()*m_data->depth()*m_data->length();
      float sz=(8.f*sz0+sz1);
      vector<string> factor(6);
      factor[0]="o";factor[1]="Kb";factor[2]="Mb";factor[3]="Gb";factor[4]="Tb";factor[5]="Pb";
      int k=0; while(k<6 && floor(sz/pow(1024.,k))!=0){k++;}    
      m_logger->info("ndsafir: dataset of size (%dx%dx%dx%dx%d)x%d (%.2f%s)\n",
		     m_data->width(),m_data->height(),m_data->depth(),
		     m_data->spectrum(),m_data->length(),m_data->number_of_iterations(),
		     sz/pow(1024.,k-1),factor[k-1].c_str());
    }
  }

  //! Initalize the progress bar using the pipeline information
  void init_progress_bar(){
    int n=0;
    vector<Element*>::iterator element;
    for (element=m_pipeline.begin();element!=m_pipeline.end();++element){
      //printf("%s:%d %d\n",(*element)->get_name(),(*element)->get_number_of_steps(),n);
      n+=(*element)->get_number_of_steps();
    }
    m_progress_bar->set_number_of_steps(n);
  }

  //! Run the pipeline
  void run(){
    try{
      m_logger->info("ndsafir: starting\n");
      vector<Element*>::iterator element;
      for (element=m_pipeline.begin();element!=m_pipeline.end();++element){
	(*element)->apply();
      }
      m_t1=cimg::time();
      m_logger->info("ndsafir: etime %dms\n",m_t1-m_t0);
      m_logger->info("ndsafir: elapsed time %s\n",etime_str().c_str());
      m_progress_bar->set_label("done ("+etime_str()+")");
#if cimg_display==1 || cimg_display==2 
      if (0){
	CImgList<> img;
	int t=(int)(m_data->length()/2);
	img.push_back(m_data->get_shared_estimate(t,0));
	img.push_back(m_data->get_shared_estimate(t,m_data->number_of_iterations()-1)); 
	img.get_append('x').display("result",false);
      }
#endif
    }catch(CImgException e){
      m_logger->error("ndsafir: error occured while processing.\n");
      exit(1);
    }
  }

private:
  //! A logger
  Log* m_logger;
  //! A progress bar
  ProgressBar* m_progress_bar;
  //! The dataset
  Data* m_data;
  //! The processing pipeline
  vector<Element*> m_pipeline;
  //! Start time
  time_t m_starttime;
  //! End time
  time_t m_endtime;
  //! start clock
  unsigned int m_t0;
  //! end clock
  unsigned int m_t1;
};


int main(int argc, char *argv[]) {
  Cli cli;
  cli.parse0(argc,argv);
  cli.init_progress_bar();
  cli.run();  
  return 0;
}

