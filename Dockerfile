FROM ubuntu:20.04

WORKDIR /app

COPY . /app

RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install python3 && \
    apt-get -y install cmake && \
    apt-get -y install g++ && \
    apt-get -y install libtiff-dev && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    cp /app/ndsafir_series.py /app/build/bin/ndsafir_series.py

ENV PATH="/app/build/bin:$PATH"

CMD ["bash"]
