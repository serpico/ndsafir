############################################################
#
# $Id$
#
# Copyright (c) ndsafir 2020
#
# AUTHOR:
# Sylvain Prigent
# 

## #################################################################
## Doxygen
## #################################################################

find_package(Doxygen QUIET)
if(${DOXYGEN_FOUND})
  set(ndsafir_USE_DOXYGEN 1)
endif(${DOXYGEN_FOUND})

## #################################################################
## CImg
## #################################################################
## Cimg dependencies
option (USE_DISPLAY_LIBS "Set to off if your application does not use any display (=> cimg_diplay=0)" OFF) # EDIT
option (USE_JPEG_LIBS "Set to off if you don't need libjpeg (=> cimg_use_jpeg undefined)" OFF) # EDIT
option (USE_PNG_LIBS "Set to off if you don't need libpng (=> cimg_use_png undefined)" OFF) # EDIT
option (USE_TIFF_LIBS "Set to off if you don't need libtiff (=> cimg_use_tiff undefined)" ON) # EDIT
option (USE_LAPACK_LIBS "Set to off if you don't need lapack libraries (=> cimg_use_lapack undefined)" OFF) # EDIT
option (USE_BOARD_LIBS "Set to off if you don't need libboard (=> cimg_use_board undefined)" OFF) # EDIT
option (USE_FFMPEG_LIBS "Set to off if you don't need libffmpeg (=> cimg_use_ffmpeg undefined)" OFF) # EDIT
option (USE_FFTW3_LIBS "Set to off if you don't need libfftw3 (=> cimg_use_fftw3 undefined)" OFF) # EDIT
option (USE_MAGICK_LIBS "Set to off if you don't need libmagick++ (=> cimg_use_magick undefined)" OFF) # EDIT
# Call findCIMG.cmake and use the variables defined by it
find_package (CIMG REQUIRED )
add_definitions (${CIMG_DEFINITIONS})
add_definitions (-Dcimg_use_vt100)
include_directories (${CIMG_INCLUDE_DIRS})
set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${CIMG_C_FLAGS}")
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CIMG_CXX_FLAGS}")

