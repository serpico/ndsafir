# INTRO

NdSAFIR is a 3D and 4D image denoising

J. Boulanger. Non-parametric estimation and contributions to image sequence analysis: Modeling, simulation and estimation of the intracellular traffic in video-microscopy image sequences. Université de Rennes 1, Mention Traitement du Signal et des Télécommunications, Jan 2007.



# COMPILATION

```
mkdir build
cd build
cmake ..
make
```


# USAGE

for 3D data:

```
ndsafir -i /path/to/image.tif -o path/to/output/image.tif -v -noise 2 -nf 2 -2dt -iter 3
```

for 3D+t you need to write a txt file with the list of image (one image per row) and run `ndsafir` with the txt file as input

```
ndsafir -i input.txt -o output.txt 
```


